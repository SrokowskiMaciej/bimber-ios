//
// Created by Maciej Srokowski on 4/15/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import RxSwift

struct UserDeeplinkRepository {

    private static let deeplinkProfileKey = "deeplinkProfileKey"

    public func setDeeplinkUser(profileId: UserId) {
        let profileDeepLinks: Array = [profileId]
        UserDefaults.standard.set(profileDeepLinks, forKey: UserDeeplinkRepository.deeplinkProfileKey)
    }

    public func clearDeeplinkUser() {
        UserDefaults.standard.set([], forKey: UserDeeplinkRepository.deeplinkProfileKey)
    }

    public func onDeeplinkUser() -> Observable<[UserId]> {
        return UserDefaults.standard.rx.observe(Array<UserId>.self, UserDeeplinkRepository.deeplinkProfileKey)
                .map { deeplink in
                    if let existingDeeplink = deeplink {
                        return existingDeeplink
                    } else {
                        return []
                    }
                }
    }
}
