//
// Created by Maciej Srokowski on 4/16/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import RxSwift

struct RecentlyRejectedUserRepository {

    private let recentlyRejectedUser = BehaviorSubject<[ReversibleUser]>(value: [])

    func setRecentlyRejectedUser(rejectedUser: NearbyUserData) {
        recentlyRejectedUser.on(.next([ReversibleUser.rejected(user: rejectedUser)]))
    }

    func setRevertedUser(revertedUser: NearbyUserData) {
        recentlyRejectedUser.on(.next([ReversibleUser.reverted(user: revertedUser)]))
    }

    func clear() {
        recentlyRejectedUser.on(.next([]))
    }

    func onReversibleUser() -> Observable<[ReversibleUser]> {
        return recentlyRejectedUser
    }
}

enum ReversibleUser {
    case rejected(user: NearbyUserData)
    case reverted(user: NearbyUserData)
}
