//
// Created by Maciej Srokowski on 4/16/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import RxSwift

class NearbyDiscoverableUsersRepository {

    private let networkSource: NearbyDiscoverableUsersNetworkSource
    private var inMemorySource: NearbyDiscoverableUsersInMemorySource

    init(networkSource: NearbyDiscoverableUsersNetworkSource, inMemorySource: NearbyDiscoverableUsersInMemorySource) {
        self.networkSource = networkSource
        self.inMemorySource = inMemorySource
    }

    private let locationRequest = ReplaySubject<LocationRequest>.create(bufferSize: 1)
    private let moreUsersRequest = ReplaySubject<Entity>.create(bufferSize: 1)

    private lazy var nearbyDiscoverableUsers: Observable<Result> = createLoadingStateObservable(networkSource, inMemorySource)


    private func createLoadingStateObservable(_ networkSource: NearbyDiscoverableUsersNetworkSource, _ inMemorySource: NearbyDiscoverableUsersInMemorySource) -> Observable<Result> {
        return self.locationRequest
                .distinctUntilChanged()
                .flatMapLatest { (request: LocationRequest) -> Observable<Result> in
                    self.inMemorySource.clear()
                    return self.onUsersIn(location: request)
                }
                .debug()
                .distinctUntilChanged()
                .share(replay: 1, scope: .whileConnected)
    }

    private func onUsersIn(location request: LocationRequest) -> Observable<Result> {
        return networkSource.nearbyDiscoverableUsers(forUser: request.uId,
                                                     atLatitude: request.latitude,
                                                     atLongitude: request.longitude,
                                                     inRange: request.range)
                .flatMapLatest { (networkResult: NearbyDiscoverableUsersNetworkSource.Result) -> Observable<Result> in
                    switch networkResult {
                    case .loading:
                        return self.inMemorySource.onDiscoverableUsers()
                                .map({ users in Result.loading(users: users.users) })

                    case .success(let users):
                        return self.inMemorySource.onDiscoverableUsers(withNewUIds: users.discoverableUsers)
                                .takeWhile { discoverableUsers in
                                    discoverableUsers.users.count > 5 || discoverableUsers.depleted
                                }
                                .concatMap({ discoverableUsers -> Observable<Result> in
                                    if discoverableUsers.depleted {
                                        return self.inMemorySource.onDiscoverableUsers()
                                                .map({ users in Result.success(users: users.users, noMoreUsers: users.depleted) })
                                    } else {
                                        return self.onUsersIn(location: request)
                                    }
                                })
                    case .error:
                        return self.inMemorySource.onDiscoverableUsers()
                                .map({ users in Result.error(users: users.users) })
                    }
                }

    }

    func onNearbyDiscoverableUsers(withLocation request: LocationRequest) -> Observable<Result> {
        self.locationRequest.onNext(request)
        return self.nearbyDiscoverableUsers
    }

    func removeUser(uId: String) {
        self.inMemorySource.removeUser(uId: uId)
    }

    struct LocationRequest: Equatable {
        let uId: UserId
        let latitude: Double
        let longitude: Double
        let range: Int
    }

    enum Result: Equatable {
        case loading(users: [UserId])
        case success(users: [UserId], noMoreUsers: Bool)
        case error(users: [UserId])

        func availableUsers() -> [UserId] {
            switch self {
            case .loading(let users): return users
            case .success(let users, _): return users
            case .error(let users): return users
            }
        }
    }

}

