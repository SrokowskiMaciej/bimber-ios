//
// Created by Maciej Srokowski on 4/16/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire

struct NearbyDiscoverableUsersNetworkSource {

    let configuration: Configuration

    func nearbyDiscoverableUsers(forUser uId: UserId, atLatitude latitude: Double, atLongitude longitude: Double, inRange range: Int) -> Observable<Result> {
        let url = configuration.environment.baseURL() + "nearbyDiscoverableUsers/" + uId
        return RxAlamofire.requestJSON(.get, url, parameters: ["latitude": latitude, "longitude": longitude, "range": range])
                .map { response in
                    return try Result.success(users:JSONDecoder().decode(NearbyDiscoverableUsers.self, from: JSONSerialization.data(withJSONObject: response.1)))
                }
                .catchError({ error in Observable.just(Result.error(error: error)) })
                .startWith(Result.loading)
    }


    public enum Result {
        case loading
        case success(users: NearbyDiscoverableUsers)
        case error(error: Error)
    }
}

public struct NearbyDiscoverableUsers: Codable {
    let discoverableUsers: [String]
}
