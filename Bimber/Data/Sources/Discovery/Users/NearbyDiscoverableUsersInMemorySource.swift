//
// Created by Maciej Srokowski on 4/16/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import RxSwift

struct NearbyDiscoverableUsersInMemorySource {

    private let discoverabilityChange = BehaviorSubject(value: Entity.instance)

    private var depleted: Bool = false
    private var discoverableUsers = [UserId: Discoverability]()

    mutating func removeUser(uId: UserId) {
        discoverableUsers[uId] = .nonDiscoverable
        discoverabilityChange.on(.next(.instance))
    }

    mutating func clear() {
        discoverableUsers.removeAll()
        depleted = false
        discoverabilityChange.on(.next(.instance))
    }

    mutating func onDiscoverableUsers(withNewUIds newUIds: [UserId]? = nil) -> Observable<DiscoverableUsers> {
        if let existingNewUIds = newUIds {
            var changed = false
            existingNewUIds.forEach { uId in
                if (!discoverableUsers.contains(where: { element in element.key == uId })) {
                    discoverableUsers[uId] = .discoverable
                    changed = true
                }
            }
            self.depleted = !changed
        }

        let currentDiscoverableUsers = self.discoverableUsers
        let currentDepleted = self.depleted
        return self.discoverabilityChange
                .map { changeEvent -> DiscoverableUsers in
            let discoverableUsers: [UserId: Discoverability] = currentDiscoverableUsers
                    .filter { (key: UserId, value: Discoverability) in
                value == .discoverable
            }
            return DiscoverableUsers(users: Array(discoverableUsers.keys), depleted: currentDepleted)
        }
    }

    private enum Discoverability {
        case discoverable
        case nonDiscoverable
    }

    private struct DiscoverabilityStatuses {
        let users: [UserId: Discoverability]
        let depleted: Bool
    }

    struct DiscoverableUsers {
        let users: [UserId]
        let depleted: Bool
    }
}
