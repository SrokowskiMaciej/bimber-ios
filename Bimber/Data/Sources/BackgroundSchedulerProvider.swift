//
// Created by Maciek on 16/02/2018.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import RxSwift

public class BackgroundSchedulerProvider {

    public let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.background)

    public var backgroundScheduler: SchedulerType {
        return ConcurrentDispatchQueueScheduler.init(qos: DispatchQoS.background)
    }
}
