//
// Created by Maciej Srokowski on 4/19/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import FirebaseDatabase
import RxSwift


struct PersonEvaluationsNetworkSource {


    static let personEvaluationsNode = "evaluated_persons"

    let db: Database


    public func onEvaluatedPerson(evaluatingUserId: UserModelId, evaluatedPersonUid: UserModelId) -> Observable<[PersonEvaluationModel]> {
        return self.db.reference(withPath: PersonEvaluationsNetworkSource.personEvaluationsNode)
                .child(evaluatingUserId.value)
                .child(evaluatedPersonUid.value)
                .rxObserve(eventType: .value)
                .mapChildren({ snapshot -> PersonEvaluationModel in try PersonEvaluationModel(snapshot: snapshot) })
    }

    public func setEvaluatedPerson(evaluatingUserId: UserModelId, evaluatedPersonUid: UserModelId, status: PersonEvaluationStatus) -> Completable {
        return self.db.reference(withPath: PersonEvaluationsNetworkSource.personEvaluationsNode)
                .child(evaluatingUserId.value)
                .child(evaluatedPersonUid.value)
                .child("status")
                .rxSet(value: status.rawValue)
    }

}
