//
// Created by Maciej Srokowski on 4/19/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import RxSwift

struct PersonEvaluationRepository {
    let networkSource: PersonEvaluationsNetworkSource

    public func onEvaluatedPerson(evaluatingUserId: UserId, evaluatedPersonUid: UserId) -> Observable<PersonEvaluation> {
        return self.networkSource.onEvaluatedPerson(evaluatingUserId: UserModelId(key: evaluatingUserId), evaluatedPersonUid: UserModelId(key: evaluatedPersonUid))
                .map { evaluations -> PersonEvaluation in
                    if let evaluation = evaluations.first {
                        return PersonEvaluation(evaluatingUser: evaluatingUserId, evaluatedUser: evaluatedPersonUid, evaluation)

                    } else {
                        return PersonEvaluation(evaluatingPersonId: evaluatingUserId, evaluatedPersonId: evaluatedPersonUid, dialogId: nil, status: .none)
                    }
                }
    }

    public func setEvaluatedPerson(evaluatingUserId: UserId, evaluatedPersonUid: UserId, status: PersonEvaluationStatus) -> Completable {
        return self.networkSource.setEvaluatedPerson(evaluatingUserId: UserModelId(key: evaluatingUserId),
                evaluatedPersonUid: UserModelId(key: evaluatedPersonUid),
                status: status)
    }
}
