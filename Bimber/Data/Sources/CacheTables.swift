//
//  CacheTables.swift
//  Bimber
//
//  Created by Maciek on 03.12.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import GRDB

public struct CacheTables {
    public static let usersTable = "users"
    public static let userPhotosTable = "users_photos"
    public static let userFavouriteTreatsTable = "users_favourite_treats"
    public static let userFavouritePlacesTable = "users_favourite_places"
    public static let userLocationTable = "users_location"
}

public func createCacheTables(_ databaseConnection: DatabaseQueue) throws {
    try createUserTable(databaseConnection)
    try createUserPhotosTable(databaseConnection)
    try createUserFavouriteTreatsTable(databaseConnection)
    try createUserFavouritePlacesTable(databaseConnection)
    try createUserLocationTable(databaseConnection)
}

private func createUserTable(_ databaseConnection: DatabaseQueue) throws {
    try databaseConnection.inDatabase({ db in
        try db.create(table: CacheTables.usersTable, ifNotExists: true, body: { tableDefinition in
            tableDefinition.column("uId", .text).primaryKey().notNull()
            tableDefinition.column("displayName", .text).notNull()
            tableDefinition.column("fullName", .text).notNull()
            tableDefinition.column("about", .text).notNull()
            tableDefinition.column("gender", .text).notNull()
            tableDefinition.column("friends", .integer).defaults(to: 0)
            tableDefinition.column("parties", .integer).defaults(to: 0)
            tableDefinition.column("age", .integer).notNull()
            tableDefinition.column("existing", .boolean).notNull()
        })
    })
}

private func createUserPhotosTable(_ databaseConnection: DatabaseQueue) throws {
    try databaseConnection.inDatabase({ db in
        try db.create(table: CacheTables.userPhotosTable, ifNotExists: true, body: { tableDefinition in
            tableDefinition.column("photoId", .text).notNull()
            tableDefinition.column("photoUserId", .text).notNull()
            tableDefinition.column("userPhotoIndex", .integer).notNull()
            tableDefinition.column("userPhotoUri", .text)
            tableDefinition.column("userPhotoThumb", .text)
            tableDefinition.column("userPhotoUploadStatus", .text).notNull()
            tableDefinition.primaryKey(["photoUserId", "userPhotoIndex"], onConflict: .replace)
            tableDefinition.foreignKey(["photoUserId"], references: CacheTables.usersTable, columns: ["uId"])
        })
    })
}

private func createUserFavouriteTreatsTable(_ databaseConnection: DatabaseQueue) throws {
    try databaseConnection.inDatabase({ db in
        try db.create(table: CacheTables.userFavouriteTreatsTable, ifNotExists: true, body: { tableDefinition in
            tableDefinition.column("favouriteTreatUserId", .text).notNull()
            tableDefinition.column("favouriteTreatId", .text).notNull()
            tableDefinition.column("favouriteTreatIndex", .integer).notNull()
            tableDefinition.column("favouriteTreatName", .text).notNull()
            tableDefinition.primaryKey(["favouriteTreatUserId", "favouriteTreatIndex"], onConflict: .replace)
            tableDefinition.foreignKey(["favouriteTreatUserId"], references: CacheTables.usersTable, columns: ["uId"])
        })
    })
}

private func createUserFavouritePlacesTable(_ databaseConnection: DatabaseQueue) throws {
    try databaseConnection.inDatabase({ db in
        try db.create(table: CacheTables.userFavouritePlacesTable, ifNotExists: true, body: { tableDefinition in
            tableDefinition.column("favouritePlaceUserId", .text).notNull()
            tableDefinition.column("favouritePlaceId", .text).notNull()
            tableDefinition.column("favouritePlaceIndex", .integer).notNull()
            tableDefinition.column("favouritePlaceName", .text).notNull()
            tableDefinition.column("favouritePlaceAddress", .text).notNull()
            tableDefinition.column("favouritePlaceGoogleMapsId", .text).notNull()
            tableDefinition.column("favouritePlaceLatitude", .double).notNull()
            tableDefinition.column("favouritePlaceLongitude", .double).notNull()
            tableDefinition.primaryKey(["favouritePlaceUserId", "favouritePlaceIndex"], onConflict: .replace)
            tableDefinition.foreignKey(["favouritePlaceUserId"], references: CacheTables.usersTable, columns: ["uId"])
        })
    })
}

private func createUserLocationTable(_ databaseConnection: DatabaseQueue) throws {
    try databaseConnection.inDatabase({ db in
        try db.create(table: CacheTables.userLocationTable, ifNotExists: true, body: { tableDefinition in
            tableDefinition.column("locationUserId", .text).primaryKey().notNull()
            tableDefinition.column("userLocationType", .text).notNull()
            tableDefinition.column("userLocationLatitude", .double).notNull()
            tableDefinition.column("userLocationLongitude", .double)
            tableDefinition.column("userLocationRange", .integer)
            tableDefinition.column("userLocationName", .text)
            tableDefinition.foreignKey(["locationUserId"], references: CacheTables.usersTable, columns: ["uId"])
        })
    })
}
