//
//  UserPhotoRepository.swift
//  Bimber
//
//  Created by Maciek on 05.12.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import RxSwift

public struct FavouritePlacesRepository {
    public let localSource: FavouritePlacesLocalSource
    public let networkSource: FavouritePlacesNetworkSource
    public let backgroundSchedulerProvider: BackgroundSchedulerProvider

    public func getFavouritePlacesNetworkToCachePipe(ofUser uId: UserId) -> Observable<Entity> {
        return self.networkSource.onFavouritePlaces(ofUser: UserModelId(key: uId))
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler)
                .observeOn(backgroundSchedulerProvider.backgroundScheduler)
                .do(onNext: { places in
                    if (places.isEmpty) {
                        try self.localSource.remove(user: uId)
                    } else {
                        try self.localSource.set(user: uId, favouritePlaces: places.enumerated().map { (offset, model) -> FavouritePlace in
                            FavouritePlace(UserModelId(key: uId), model, index: offset)
                        })
                    }
                })
                .retry(3)
                .listen()
    }

    public func onFavouritePlaces(ofUser uId: UserId) -> Observable<[FavouritePlace]> {
        let pipe = getFavouritePlacesNetworkToCachePipe(ofUser: uId)
        let localSource = self.localSource.onFavouritePlaces(ofUser: uId)
        return Observable.combineLatest(pipe, localSource, resultSelector: { pipe, local in local })
    }

}
