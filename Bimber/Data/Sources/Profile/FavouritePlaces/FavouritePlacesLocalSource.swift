//
//  UsersLocalSource.swift
//  Bimber
//
//  Created by Maciek on 03.12.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import GRDB
import RxGRDB
import RxSwift

public struct FavouritePlacesLocalSource {
    public let db: DatabaseQueue

    public func onFavouritePlaces(ofUser uId: UserId) -> Observable<[FavouritePlace]> {
        let request = FavouritePlace.filter(Column("favouritePlaceUserId") == uId)
        return request.rx.fetchAll(in: self.db, distinctUntilChanged: true)
    }

    public func set(user: UserId, favouritePlaces: [FavouritePlace]) throws {
        try db.inTransaction { database in
            try database.execute("DELETE FROM \(CacheTables.userFavouritePlacesTable) WHERE favouritePlaceUserId = ? AND favouritePlaceIndex >= ?", arguments: [user, favouritePlaces.count])
            try favouritePlaces.forEach { place in
                try place.insert(database)
            }
            return .commit
        }
    }

    public func remove(user: UserId) throws {
        try db.inDatabase { database in
            try database.execute("DELETE FROM \(CacheTables.userFavouritePlacesTable) WHERE favouritePlaceUserId = ?", arguments: [user])
        }
    }
}


