//
//  FavouritePlaceRepository.swift
//  Bimber
//
//  Created by Maciek on 27.10.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import FirebaseDatabase
import RxSwift


public struct FavouritePlacesNetworkSource {

    static let favouritePlacesNode = "favourite_places"

    let db: Database

    init(db: Database) {
        self.db = db
    }

    public func onFavouritePlaces(ofUser uId: UserModelId) -> Observable<[FavouritePlaceModel]> {
        return self.db.reference(withPath: FavouritePlacesNetworkSource.favouritePlacesNode).child(uId.value).rxObserve(eventType: .value)
                .mapChildren { snapshot in
                    try FavouritePlaceModel(snapshot: snapshot)
                }
    }

    public func update(favouritePlaces: [FavouritePlaceModel], ofUser uId: UserModelId) -> Completable {
        let favouritePlacesValues = favouritePlaces.reduce(into: [String: [AnyHashable: Any]](), { (dictionary, favouritePlace) in
            dictionary[favouritePlace.key.value] = favouritePlace.toFirebaseValue()
        })
        return self.db.reference(withPath: FavouritePlacesNetworkSource.favouritePlacesNode).child(uId.value).childByAutoId().rxSet(values: favouritePlacesValues)
    }
}
