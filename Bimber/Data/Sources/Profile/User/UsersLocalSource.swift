//
//  UsersLocalSource.swift
//  Bimber
//
//  Created by Maciek on 03.12.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import GRDB
import RxGRDB
import RxSwift

public struct UsersLocalSource {
    public let db: DatabaseQueue

    public func on(_ uId: UserId) -> Observable<[User]> {
        let request = User.filter(Column("uId") == uId).limit(1)
        return request.rx.fetchAll(in: self.db, distinctUntilChanged: true)
    }

    public func set(user: User) throws {
        try db.inDatabase({ db in
            try user.save(db)
        })
    }
}


