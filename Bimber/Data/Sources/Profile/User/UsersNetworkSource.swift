//
//  UsersNetworkSource.swift
//  Bimber
//
//  Created by Maciek on 04.09.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import RxSwift
import FirebaseDatabase

public struct UsersNetworkSource {

    public let db: Database

    public func on(uId: UserModelId) -> Observable<[UserModel]> {
        return self.db.reference(withPath: "users").child(uId.value).rxObserve(eventType: .value)
                .mapValue { snapshot in
                    try UserModel(snapshot: snapshot)
                }
    }

    public func setAbout(ofUser uId: UserModelId, about: String) -> Completable {
        return self.db.reference(withPath: "users").child(uId.value).child("about").rxSet(value: about)
    }
}
