//
//  UsersRepository.swift
//  Bimber
//
//  Created by Maciek on 03.12.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import RxSwift
import CocoaLumberjack

public struct UsersRepository {
    public let defaultValues: DefaultValues
    public let localSource: UsersLocalSource
    public let networkSource: UsersNetworkSource
    public let backgroundSchedulerProvider: BackgroundSchedulerProvider

    public func getNetworkToCachePipe(ofUser uId: UserId) -> Observable<Entity> {
        return self.networkSource.on(uId: UserModelId(key: uId))
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler)
                .observeOn(backgroundSchedulerProvider.backgroundScheduler)
                .do(onNext: { users in
                    if let user = users.first {
                        try self.localSource.set(user: User(user))
                    } else {
                        try self.localSource.set(user: self.defaultValues.deletedUser(uId: uId))
                    }
                }, onError: { error in
                    try self.localSource.set(user: self.defaultValues.defaultUser(uId: uId))
                })
                .retry(3)
                .listen()
    }

    public func on(uId: UserId) -> Observable<[User]> {
        let pipe = self.getNetworkToCachePipe(ofUser: uId)

        let localSource = self.localSource.on(uId)
                .filter({ users in !users.isEmpty })

        return Observable.combineLatest(pipe, localSource, resultSelector: { network, local in local })
    }

}
