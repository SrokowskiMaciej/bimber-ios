//
// Created by Maciej Srokowski on 4/17/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import RxSwift
import RxGRDB

import GRDB

struct UserProfileLocalSource {

    let db: DatabaseQueue
    let backgroundSchedulerProvider: BackgroundSchedulerProvider

    static let sql = "SELECT * FROM users, users_photos WHERE uId = ? AND photoUserId = ? ORDER BY userPhotoIndex  LIMIT 1"


    public func on(_ uId: UserId) -> Observable<UserProfile> {
        let userRequest = User.filter(Column("uId") == uId).limit(1)
        let photoRequest = UserPhoto.filter(Column("photoUserId") == uId).limit(1)
        return db.rx.fetchTokens(in: [userRequest, photoRequest], startImmediately: false)
                .mapFetch { database in
                    let profile: UserProfile? = try UserProfile.fetchComponent(uId, database)
                    return profile
                }
                .flatMap(ignoreNil)
    }


}

private extension UserProfile {


    static func fetchComponent(_ uId: UserId, _ db: Database) throws -> UserProfile? {
        return try SQLRequest(UserProfileLocalSource.sql, arguments: [uId, uId]).adapted { database in
                    let adapters = try splittingRowAdapters(columnCounts: [
                        User.numberOfSelectedColumns(database),
                        UserPhoto.numberOfSelectedColumns(database)])
                    return ScopeAdapter([
                        "user": adapters[0],
                        "photo": adapters[1]])
                }
                .asRequest(of: UserProfile.self)
                .fetchOne(db)

    }

}
