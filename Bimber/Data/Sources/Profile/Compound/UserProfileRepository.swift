//
// Created by Maciej Srokowski on 4/17/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import RxSwift

struct UserProfileRepository {

    let usersRepository: UsersRepository
    let photosRepository: UserPhotosRepository
    let localSource: UserProfileLocalSource

    func getNetworkToCachePipe(ofUser uId: UserId) -> Observable<Entity> {
        return Observable.combineLatest(usersRepository.getNetworkToCachePipe(ofUser: uId), photosRepository.getProfilePhotoNetworkToCachePipe(ofUser: uId),
                resultSelector: { _, _ in Entity.instance })

    }

    func onProfileData(ofUser uId: UserId) -> Observable<UserProfile> {
        let pipe: Observable<Entity> = getNetworkToCachePipe(ofUser: uId)
        let localSource: Observable<UserProfile> = self.localSource.on(uId)

        return Observable.combineLatest(pipe, localSource, resultSelector: { network, local in local })
    }
}
