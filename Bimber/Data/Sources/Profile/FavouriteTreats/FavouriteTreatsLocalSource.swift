//
//  UsersLocalSource.swift
//  Bimber
//
//  Created by Maciek on 03.12.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import GRDB
import RxGRDB
import RxSwift

public struct FavouriteTreatsLocalSource {
    public let db: DatabaseQueue

    public func onFavouriteTreats(ofUser uId: UserId) -> Observable<[FavouriteTreat]> {
        let request = FavouriteTreat.filter(Column("favouriteTreatUserId") == uId)
        return request.rx.fetchAll(in: self.db, distinctUntilChanged: true)
    }

    public func set(user: UserId, favouriteTreats: [FavouriteTreat]) throws {
        try db.inTransaction { database in
            try database.execute("DELETE FROM \(CacheTables.userFavouriteTreatsTable) WHERE favouriteTreatUserId = ? AND favouriteTreatIndex >= ?", arguments: [user, favouriteTreats.count])
            try favouriteTreats.forEach { treat in
                try treat.insert(database)
            }
            return .commit
        }
    }

    public func remove(user: UserId) throws {
        try db.inDatabase { database in
            try database.execute("DELETE FROM \(CacheTables.userFavouriteTreatsTable) WHERE favouriteTreatUserId = ?", arguments: [user])
        }
    }
}


