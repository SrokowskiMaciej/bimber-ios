//
//  FavouriteTreatsNetworkSource.swift
//  Bimber
//
//  Created by Maciek on 27.10.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import FirebaseDatabase
import RxSwift


public struct FavouriteTreatsNetworkSource {

    static let favouriteTreatsNode = "alcohols"

    let db: Database

    init(db: Database) {
        self.db = db
    }

    public func on(ofUser uId: UserModelId) -> Observable<[FavouriteTreatModel]> {
        return self.db.reference(withPath: FavouriteTreatsNetworkSource.favouriteTreatsNode).child(uId.value).rxObserve(eventType: .value)
                .mapChildren { snapshot in
                    try FavouriteTreatModel(snapshot: snapshot)
                }
    }

    public func update(favouriteTreats: [FavouriteTreatModel], ofUser uId: UserModelId) -> Completable {
        let favouriteTreatsValues = favouriteTreats.reduce(into: [String: [AnyHashable: Any]](), { (dictionary, favouriteTreat) in
            dictionary[favouriteTreat.key.value] = favouriteTreat.toFirebaseValue()
        })
        return self.db.reference(withPath: FavouriteTreatsNetworkSource.favouriteTreatsNode).child(uId.value).childByAutoId().rxSet(values: favouriteTreatsValues)
    }
}
