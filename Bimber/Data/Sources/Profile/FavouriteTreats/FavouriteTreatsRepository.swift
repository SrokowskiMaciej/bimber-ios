//
//  UserPhotoRepository.swift
//  Bimber
//
//  Created by Maciek on 05.12.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import RxSwift

public struct FavouriteTreatsRepository {
    public let localSource: FavouriteTreatsLocalSource
    public let networkSource: FavouriteTreatsNetworkSource
    public let backgroundSchedulerProvider: BackgroundSchedulerProvider

    public func getFavouriteTreatsNetworkToCachePipe(ofUser uId: UserId) -> Observable<Entity> {
        return self.networkSource.on(ofUser: UserModelId(key: uId))
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler)
                .observeOn(backgroundSchedulerProvider.backgroundScheduler)
                .do(onNext: { treats in
                    if (treats.isEmpty) {
                        try self.localSource.remove(user: uId)
                    } else {
                        try self.localSource.set(user: uId, favouriteTreats: treats.enumerated().map { (offset, model) -> FavouriteTreat in
                            FavouriteTreat(UserModelId(key: uId), model, index: offset)
                        })
                    }
                })
                .retry(3)
                .listen()
    }

    public func onFavouriteTreats(ofUser uId: UserId) -> Observable<[FavouriteTreat]> {
        let pipe = getFavouriteTreatsNetworkToCachePipe(ofUser: uId)
        let localSource = self.localSource.onFavouriteTreats(ofUser: uId)
        return Observable.combineLatest(pipe, localSource, resultSelector: { pipe, local in local })
    }

}
