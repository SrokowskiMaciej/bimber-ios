//
//  UsersLocalSource.swift
//  Bimber
//
//  Created by Maciek on 03.12.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import GRDB
import RxGRDB
import RxSwift

public struct UserPhotosLocalSource {
    public let db: DatabaseQueue

    public func onProfilePhoto(ofUser uId: UserId) -> Observable<[UserPhoto]> {
        let request = UserPhoto.filter(Column("photoUserId") == uId).limit(1)
        return request.rx.fetchAll(in: self.db, distinctUntilChanged: true)
    }

    public func onAllPhotos(ofUser uId: UserId) -> Observable<[UserPhoto]> {
        let request = UserPhoto.filter(Column("photoUserId") == uId)
        return request.rx.fetchAll(in: self.db, distinctUntilChanged: true)
    }

    public func insert(photo: UserPhoto) throws {
        try db.inDatabase({ db in
            try photo.save(db)
        })
    }

    public func set(user: UserId, photos: [UserPhoto]) throws {
        try db.inTransaction { database in
            try database.execute("DELETE FROM \(CacheTables.userPhotosTable) WHERE photoUserId = ? AND userPhotoIndex >= ?", arguments: [user, photos.count])
            try photos.forEach { photo in
                try photo.insert(database)
            }
            return .commit
        }
    }
}


