//
//  UserPhotoNetworkSource.swift
//  Bimber
//
//  Created by Maciek on 27.10.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import FirebaseDatabase
import RxSwift

public struct UserPhotoNetworkSource {

    static let userPhotosNode = "photos"

    let db: Database

    init(db: Database) {
        self.db = db
    }

    public func generatePhotoId(uid: String) -> String {
        return self.db.reference(withPath: UserPhotoNetworkSource.userPhotosNode).child(uid).childByAutoId().key
    }

    public func on(ofUser uId: UserModelId, limit: Int? = nil) -> Observable<[UserPhotoModel]> {
        var query = self.db.reference(withPath: UserPhotoNetworkSource.userPhotosNode)
                .child(uId.value)
                .queryOrdered(byChild: IndexedConstants.indexField)

        if let validLimit = limit {
            query = query.queryLimited(toFirst: UInt(validLimit))
        }

        return query.rxObserve(eventType: .value)
                .mapChildren { snapshot in
                    try UserPhotoModel(snapshot: snapshot)
                }
    }

    public func add(photo: UserPhotoModel, ofUser uId: UserModelId) -> Completable {
        return self.db.reference(withPath: UserPhotoNetworkSource.userPhotosNode).child(uId.value).childByAutoId().rxSet(values: photo.toFirebaseValue())
    }

    public func update(photos: [UserPhotoModel], ofUser uId: UserModelId) -> Completable {
        let photosValues = photos.reduce(into: [String: [AnyHashable: Any]](), { (dictionary, photo) in
            dictionary[photo.key.value] = photo.toFirebaseValue()
        })
        return self.db.reference(withPath: UserPhotoNetworkSource.userPhotosNode).child(uId.value).childByAutoId().rxSet(values: photosValues)
    }

    public func update(photo: UserPhotoModelId, uri: String, uploadStatus: UploadStatus, ofUser uId: UserModelId) -> Completable {
        let childValues = [UserPhotoModel.uriFieldName: uri, UserPhotoModel.uploadStatusFieldName: uploadStatus.rawValue]
        return self.db.reference(withPath: UserPhotoNetworkSource.userPhotosNode).child(uId.value).childByAutoId().rxSet(values: childValues)
    }

    public func remove(photo photoId: UserPhotoModelId, ofUser uId: UserModelId) -> Completable {
        return self.db.reference(withPath: UserPhotoNetworkSource.userPhotosNode).child(uId.value).child(photoId.value).rxRemove()
    }

}
