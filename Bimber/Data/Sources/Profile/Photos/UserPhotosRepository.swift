//
//  UserPhotoRepository.swift
//  Bimber
//
//  Created by Maciek on 05.12.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import RxSwift

public struct UserPhotosRepository {
    public let defaultValues: DefaultValues
    public let localSource: UserPhotosLocalSource
    public let networkSource: UserPhotoNetworkSource
    public let backgroundSchedulerProvider: BackgroundSchedulerProvider

    public func getProfilePhotoNetworkToCachePipe(ofUser uId: UserId) -> Observable<Entity> {
        return self.networkSource.on(ofUser: UserModelId(key: uId), limit: 1)
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler)
                .observeOn(backgroundSchedulerProvider.backgroundScheduler)
                .do(onNext: { photos in
                    if let profilePhoto = photos.first {
                        try self.localSource.insert(photo: UserPhoto(UserModelId(key: uId), profilePhoto, index: 0))
                    } else {
                        try self.localSource.set(user: uId, photos: [self.defaultValues.defaultPhoto(ofUser: uId)])
                    }
                }, onError: { error in
                    try self.localSource.set(user: uId, photos: [self.defaultValues.defaultPhoto(ofUser: uId)])
                })
                .retry(3)
                .listen()
    }

    public func getAllPhotosNetworkToCachePipe(ofUser uId: UserId) -> Observable<Entity> {
        return self.networkSource.on(ofUser: UserModelId(key: uId))
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler)
                .observeOn(backgroundSchedulerProvider.backgroundScheduler)
                .do(onNext: { photos in
                    if (photos.isEmpty) {
                        try self.localSource.set(user: uId, photos: [self.defaultValues.defaultPhoto(ofUser: uId)])
                    } else {
                        try self.localSource.set(user: uId, photos: photos.enumerated().map { (offset, model) -> UserPhoto in
                            UserPhoto(UserModelId(key: uId), model, index: offset)
                        })
                    }
                }, onError: { error in
                    try self.localSource.set(user: uId, photos: [self.defaultValues.defaultPhoto(ofUser: uId)])
                })
                .retry(3)
                .listen()
    }

    public func onProfilePhoto(ofUser uId: UserId) -> Observable<[UserPhoto]> {
        let pipe = getProfilePhotoNetworkToCachePipe(ofUser: uId)
        let localSource = self.localSource.onProfilePhoto(ofUser: uId)
                .filter({ photos in !photos.isEmpty })
        return Observable.combineLatest(pipe, localSource, resultSelector: { pipe, local in local })
    }

    public func onAllPhotos(ofUser uId: UserId) -> Observable<[UserPhoto]> {
        let pipe = getAllPhotosNetworkToCachePipe(ofUser: uId)
        let localSource = self.localSource.onAllPhotos(ofUser: uId)
                .filter({ photos in !photos.isEmpty })
        return Observable.combineLatest(pipe, localSource, resultSelector: { pipe, local in local })
    }

}
