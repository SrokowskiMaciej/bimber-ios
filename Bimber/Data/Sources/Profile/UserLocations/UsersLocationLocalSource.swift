//
//  UsersLocalSource.swift
//  Bimber
//
//  Created by Maciek on 03.12.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import GRDB
import RxGRDB
import RxSwift

public struct UsersLocationLocalSource {
    public let db: DatabaseQueue

    public func onLocation(ofUser uId: UserModelId) -> Observable<[UserLocation]> {
        let request = UserLocation.filter(Column("locationUserId") == uId.value)
        return request.rx.fetchAll(in: self.db, distinctUntilChanged: true)
    }

    public func set(location: UserLocation) throws {
        try db.inTransaction { database in
            try database.execute("DELETE FROM \(CacheTables.userLocationTable) WHERE locationUserId = \(location.locationUserId)")
            try location.insert(database)
            return .commit
        }
    }

    public func remove(user: UserModelId) throws {
        try db.inDatabase { database in
            try database.execute("DELETE FROM \(CacheTables.userLocationTable) WHERE locationUserId = \(user.value)")
        }
    }
}


