//
//  UsersRepository.swift
//  Bimber
//
//  Created by Maciek on 03.12.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import RxSwift
import CocoaLumberjack

public struct UsersLocationRepository {
    public let localSource: UsersLocationLocalSource
    public let networkSource: UsersLocationNetworkSource
    public let backgroundSchedulerProvider: BackgroundSchedulerProvider

    public func on(uId: UserId, useCache: Bool = false) -> Observable<[UserLocation]> {
        if useCache {
            return onCached(uId: uId)
        } else {
            return onNetwork(uId: uId)
        }
    }

    private func onCached(uId: UserId) -> Observable<[UserLocation]> {
        let networkSource = self.networkSource.onLocation(ofUser: UserModelId(key: uId))
                .map { userModels in
                    userModels.map({ userLocationModel in UserLocation(userLocationModel) })
                }
                .map { userLocations in
                    if let userLocation = userLocations.first {
                        try self.localSource.set(location: userLocation)
                    } else {
                        try self.localSource.remove(user: UserModelId(key: uId))
                    }
                }
                .listen()

        let localSource = self.localSource.onLocation(ofUser: UserModelId(key: uId))

        return Observable.combineLatest(networkSource, localSource, resultSelector: { network, local in local })
    }

    private func onNetwork(uId: UserId) -> Observable<[UserLocation]> {
        return self.networkSource.onLocation(ofUser: UserModelId(key: uId))
                .map({ userModels in userModels.map({ userLocationModel in UserLocation(userLocationModel) }) })
    }


}
