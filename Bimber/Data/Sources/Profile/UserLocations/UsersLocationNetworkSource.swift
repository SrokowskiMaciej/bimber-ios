//
//  UsersLocationNetworkSource.swift
//  Bimber
//
//  Created by Maciek on 04.09.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import RxSwift
import FirebaseDatabase

public struct UsersLocationNetworkSource {

    static let userLocationsNode = "user_locations"

    let db: Database

    init(db: Database) {
        self.db = db
    }

    public func onLocation(ofUser uId: UserModelId) -> Observable<[LocationModel]> {
        return self.db.reference(withPath: UsersLocationNetworkSource.userLocationsNode).child(uId.value).rxObserve(eventType: .value)
            .mapValue { snapshot in
                try LocationModel(snapshot: snapshot)
            }
    }

    public func set(location: LocationModel) -> Completable {
        return self.db.reference(withPath: UsersLocationNetworkSource.userLocationsNode).child(location.key.value).rxSet(values: location.toFirebaseValue())
    }

    public func removeLocation(ofUser uId: UserModelId) -> Completable {
        return self.db.reference(withPath: UsersLocationNetworkSource.userLocationsNode).child(uId.value).rxRemove()
    }
}
