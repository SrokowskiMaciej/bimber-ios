//
// Created by Maciej Srokowski on 4/14/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import RxSwift
import FirebaseAuth
import FirebaseAuthUI

struct AuthService {

    public func authenticated() -> Observable<UserId?> {
        return Observable.create { (observer: AnyObserver<UserId?>) in
            Auth.auth().addStateDidChangeListener { auth, user in
                if let userId: UserId = user?.uid {
                    observer.on(.next(userId))
                } else {
                    observer.on(.next(nil))
                }
            }
            return Disposables.create()
        }
    }
}
