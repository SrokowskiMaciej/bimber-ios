//
//  Location.swift
//  Bimber
//
//  Created by Maciek on 01.09.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import FirebaseDatabase

public class FavouriteTreatModel: FirebaseValue, Indexed {

    let key: FavouriteTreatModelId
    let name: String
    let index: Int

    init(key: FavouriteTreatModelId, name: String, index: Int) {
        self.key = key
        self.name = name
        self.index = index
    }

    public required init(snapshot: DataSnapshot) throws {
        self.key = FavouriteTreatModelId(key: snapshot.key)
        guard let val = snapshot.value as? NSDictionary,
              let name = val["name"] as? String,
              let index = val["index"] as? Int else {
            throw CustomError.missingFieldError("Missing some fields in favourite treat model")
        }
        self.name = name
        self.index = index
    }

    public func toFirebaseValue() -> [AnyHashable: Any] {
        return ["name": self.name,
                "index": self.index]
    }
}

extension FavouriteTreatModel: Equatable {
    public static func ==(lhs: FavouriteTreatModel, rhs: FavouriteTreatModel) -> Bool {
        return lhs.key == rhs.key && lhs.name == rhs.name && lhs.index == rhs.index
    }
}
