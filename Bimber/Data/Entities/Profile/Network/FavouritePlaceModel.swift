//
//  LocationModel.swift
//  Bimber
//
//  Created by Maciek on 01.09.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import FirebaseDatabase

public class FavouritePlaceModel: FirebaseValue, Indexed {

    let key: FavouritePlaceModelId
    let name: String
    let address: String
    let id: String
    let latitude: Double
    let longitude: Double
    let index: Int

    init(key: FavouritePlaceModelId, name: String, address: String, id: String, latitude: Double, longitude: Double, index: Int) {
        self.key = key
        self.name = name
        self.address = address
        self.id = id
        self.latitude = latitude
        self.longitude = longitude
        self.index = index
    }

    public required init(snapshot: DataSnapshot) throws {
        self.key = FavouritePlaceModelId(key: snapshot.key)
        guard let val = snapshot.value as? NSDictionary,
              let name = val["name"] as? String,
              let address = val["address"] as? String,
              let id = val["id"] as? String,
              let latitude = val["latitude"] as? Double,
              let longitude = val["longitude"] as? Double,
              let index = val["index"] as? Int else {
            throw CustomError.missingFieldError("Missing some fields in favourite place model")
        }
        self.name = name
        self.address = address
        self.id = id
        self.latitude = latitude
        self.longitude = longitude
        self.index = index
    }

    public func toFirebaseValue() -> [AnyHashable: Any] {
        return ["name": self.name,
                "address": self.address,
                "id": self.id,
                "latitude": self.latitude,
                "longitude": self.longitude,
                "index": self.index]

    }
}

extension FavouritePlaceModel: Equatable {
    public static func ==(lhs: FavouritePlaceModel, rhs: FavouritePlaceModel) -> Bool {
        return lhs.key == rhs.key && lhs.name == rhs.name && lhs.address == rhs.address && lhs.id == rhs.id && lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude && lhs.index == rhs.index
    }
}
