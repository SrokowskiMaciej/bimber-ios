//
//  Indexed.swift
//  Bimber
//
//  Created by Maciek on 01.09.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import FirebaseDatabase

protocol FirebaseValue {
    associatedtype KeyType: FirebaseKey
    var key: KeyType { get }
    init(snapshot: DataSnapshot) throws
    func toFirebaseValue() -> [AnyHashable: Any]
}

public class FirebaseKey: Hashable {
    public var hashValue: Int {
        return value.hashValue
    }

    public static func ==(lhs: FirebaseKey, rhs: FirebaseKey) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }

    let value: String

    init(key: String) {
        self.value = key
    }
}

protocol Indexed {

    var index: Int { get }
}

struct IndexedConstants {
    static let indexField = "index"
}

public class UserModelId: FirebaseKey {
}

public class FavouritePlaceModelId: FirebaseKey {
}

public class FavouriteTreatModelId: FirebaseKey {
}

public class UserPhotoModelId: FirebaseKey {
}

public class ChatModelId: FirebaseKey {
}

