//
//  LocationModel.swift
//  Bimber
//
//  Created by Maciek on 01.09.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import FirebaseDatabase

public class UserModel: FirebaseValue {

    let key: UserModelId
    let displayName: String
    let fullName: String
    let about: String
    let gender: Gender
    let friends: Int
    let parties: Int
    let age: Int

    init(key: UserModelId, displayName: String, fullName: String, about: String, gender: Gender, friends: Int, parties: Int, age: Int) {
        self.key = key
        self.displayName = displayName
        self.fullName = fullName
        self.about = about
        self.gender = gender
        self.friends = friends
        self.parties = parties
        self.age = age
    }

    public required init(snapshot: DataSnapshot) throws {
        self.key = UserModelId(key: snapshot.key)
        guard let val = snapshot.value as? NSDictionary,
              let displayName = val["displayName"] as? String,
              let about = val["about"] as? String,
              let genderVal = val["gender"] as? String,
              let gender = Gender(rawValue: genderVal),
              let age = val["age"] as? Int else {
            throw CustomError.missingFieldError("Missing some fields in user model")
        }
        self.displayName = displayName
        self.about = about
        self.gender = gender
        self.age = age
        self.fullName = val["fullName"] as? String ?? ""
        self.parties = val["parties"] as? Int ?? 0
        self.friends = val["friends"] as? Int ?? 0
    }

    public func toFirebaseValue() -> [AnyHashable: Any] {
        return ["displayName": self.displayName,
                "fullName": self.fullName,
                "about": self.about,
                "gender": self.gender.rawValue,
                "age": self.age,
                "friends": self.friends,
                "parties": self.parties]
    }
}
