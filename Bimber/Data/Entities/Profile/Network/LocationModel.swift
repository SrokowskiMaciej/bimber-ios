//
//  LocationModel.swift
//  Bimber
//
//  Created by Maciek on 01.09.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import FirebaseDatabase

public class LocationModel: FirebaseValue {

    let key: UserModelId
    let latitude: Double
    let longitude: Double
    let locationName: String
    @available(*, deprecated)
    let personUid: UserModelId
    let range: Int
    let type: LocationType

    init(key: UserModelId, latitude: Double, longitude: Double, locationName: String, personUid: UserModelId, range: Int, type: LocationType) {
        self.key = key
        self.latitude = latitude
        self.longitude = longitude
        self.locationName = locationName
        self.personUid = personUid
        self.range = range
        self.type = type
    }

    public required init(snapshot: DataSnapshot) throws {
        self.key = UserModelId(key: snapshot.key)
        guard let val = snapshot.value as? NSDictionary,
              let latitude = val["latitude"] as? Double,
              let longitude = val["longitude"] as? Double,
              let locationName = val["locationName"] as? String,
              let range = val["range"] as? Int,
              let typeVal = val["type"] as? String,
              let type = LocationType(rawValue: typeVal) else {
            throw CustomError.missingFieldError("Missing some fields in user location model")
        }
        self.latitude = latitude
        self.longitude = longitude
        self.locationName = locationName
        self.personUid = self.key
        self.range = range
        self.type = type
    }

    public func toFirebaseValue() -> [AnyHashable: Any] {
        return ["latitude": self.latitude,
                "longitude": self.longitude,
                "locationName": self.locationName,
                "personUid": self.personUid.value,
                "range": self.range,
                "type": self.type.rawValue]

    }
}
