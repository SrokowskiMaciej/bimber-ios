//
//  LocationModel.swift
//  Bimber
//
//  Created by Maciek on 01.09.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import FirebaseDatabase

public struct UserPhotoModel: FirebaseValue, Indexed {

    public static let uriFieldName = "uri"
    public static let uploadStatusFieldName = "uploadStatus"

    typealias KeyType = UserPhotoModelId
    let key: UserPhotoModelId
    let uri: String
    let thumb: String
    let uploadStatus: UploadStatus
    let index: Int

    public init(snapshot: DataSnapshot) throws {
        self.key = UserPhotoModelId(key: snapshot.key)
        guard let val = snapshot.value as? NSDictionary,
              let uri = val[UserPhotoModel.uriFieldName] as? String,
              let thumb = val["thumb"] as? String,
              let uploadStatusValue = val[UserPhotoModel.uploadStatusFieldName] as? String,
              let uploadStatus = UploadStatus(rawValue: uploadStatusValue),
              let index = val["index"] as? Int else {
            throw CustomError.missingFieldError("Missing some fields in user photo model")
        }
        self.uri = uri
        self.thumb = thumb
        self.uploadStatus = uploadStatus
        self.index = index
    }

    public func toFirebaseValue() -> [AnyHashable: Any] {
        return [UserPhotoModel.uriFieldName: self.uri,
                UserPhotoModel.uploadStatusFieldName: self.uploadStatus.rawValue,
                "index": self.index]
    }
}


extension UserPhotoModel: Equatable {
    public static func ==(lhs: UserPhotoModel, rhs: UserPhotoModel) -> Bool {
        return lhs.key == rhs.key && lhs.uri == rhs.uri && lhs.uploadStatus == rhs.uploadStatus && lhs.index == rhs.index
    }
}
