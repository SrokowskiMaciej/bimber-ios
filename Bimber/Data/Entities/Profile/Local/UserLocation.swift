//
//  Location.swift
//  Bimber
//
//  Created by Maciek on 01.09.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import GRDB

public struct UserLocation: Equatable, Codable{

    let locationUserId: UserId
    let userLocationType: LocationType
    let userLocationLatitude: Double
    let userLocationLongitude: Double
    let userLocationRange: Int
    let userLocationName: String

}

extension UserLocation: RowConvertible, Persistable {
    public static var databaseTableName = CacheTables.userLocationTable
}

extension UserLocation {
    init(_ locationModel: LocationModel) {
        self.init(locationUserId: locationModel.key.value,
                userLocationType: locationModel.type,
                userLocationLatitude: locationModel.latitude,
                userLocationLongitude: locationModel.longitude,
                userLocationRange: locationModel.range,
                userLocationName: locationModel.locationName)
    }
}


public enum LocationType: String, Codable {
    case current = "CURRENT_LOCATION"
    case custom = "CUSTOM_LOCATION"
}