//
//  Location.swift
//  Bimber
//
//  Created by Maciek on 01.09.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import GRDB

public typealias FavouritePlaceId = String

public struct FavouritePlace: Codable, Equatable {

    let favouritePlaceUserId: UserId
    let favouritePlaceId: FavouritePlaceId
    let favouritePlaceIndex: Int
    let favouritePlaceName: String
    let favouritePlaceAddress: String
    let favouritePlaceGoogleMapsId: String
    let favouritePlaceLatitude: Double
    let favouritePlaceLongitude: Double

}

extension FavouritePlace: RowConvertible, Persistable {
    public static var databaseTableName = CacheTables.userFavouritePlacesTable
}

extension FavouritePlace {
    init(_ uId: UserModelId, _ favouritePlaceModel: FavouritePlaceModel, index: Int? = nil) {
        self.init(favouritePlaceUserId: uId.value,
                favouritePlaceId: favouritePlaceModel.key.value,
                favouritePlaceIndex: index ?? favouritePlaceModel.index,
                favouritePlaceName: favouritePlaceModel.name,
                favouritePlaceAddress: favouritePlaceModel.address,
                favouritePlaceGoogleMapsId: favouritePlaceModel.id,
                favouritePlaceLatitude: favouritePlaceModel.latitude,
                favouritePlaceLongitude: favouritePlaceModel.longitude)
    }
}