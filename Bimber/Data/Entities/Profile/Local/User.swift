//
//  User.swift
//  Bimber
//
//  Created by Maciek on 03.12.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import GRDB

public typealias UserId = String

public struct User: Equatable, Codable {

    let uId: UserId
    let displayName: String
    let fullName: String
    let about: String
    let gender: Gender
    let friends: Int
    let parties: Int
    let age: Int
    let existing: Bool

    init(uId: String, displayName: String, fullName: String, about: String, gender: Gender, friends: Int, parties: Int, age: Int, existing: Bool = true) {
        self.uId = uId
        self.displayName = displayName
        self.fullName = fullName
        self.about = about
        self.gender = gender
        self.friends = friends
        self.parties = parties
        self.age = age
        self.existing = existing
    }


}

extension User: RowConvertible, Persistable {
    public static var databaseTableName = CacheTables.usersTable
}

extension User {
    init(_ userModel: UserModel) {
        self.init(uId: userModel.key.value, displayName: userModel.displayName, fullName: userModel.fullName, about: userModel.about, gender: userModel.gender, friends: userModel.friends, parties: userModel.parties, age: userModel.age)

    }
}

public enum Gender: String, Codable {
    case male = "MALE"
    case female = "FEMALE"
}

//extension Gender: DatabaseValueConvertible { }

