//
//  Location.swift
//  Bimber
//
//  Created by Maciek on 01.09.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import GRDB

public typealias FavouriteTreatId = String

public struct FavouriteTreat: Codable, Equatable {

    let favouriteTreatUserId: UserId
    let favouriteTreatId: FavouriteTreatId
    let favouriteTreatName: String
    let favouriteTreatIndex: Int


}

extension FavouriteTreat: RowConvertible, Persistable {
    public static var databaseTableName = CacheTables.userFavouriteTreatsTable
}

extension FavouriteTreat {
    init(_ uId: UserModelId, _ favouriteTreatModel: FavouriteTreatModel, index: Int? = nil) {
        self.init(favouriteTreatUserId: uId.value,
                favouriteTreatId: favouriteTreatModel.key.value,
                favouriteTreatName: favouriteTreatModel.name,
                favouriteTreatIndex: index ?? favouriteTreatModel.index)

    }
}