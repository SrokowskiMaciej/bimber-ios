//
//  User.swift
//  Bimber
//
//  Created by Maciek on 03.12.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import GRDB

public typealias UserPhotoId = String

public struct UserPhoto: Equatable, Codable {

    let photoId: UserPhotoId
    let photoUserId: UserId
    let userPhotoIndex: Int
    let userPhotoUri: String?
    let userPhotoThumb: String?
    let userPhotoUploadStatus: UploadStatus

    init(photoId: String,
         photoUserId: String,
         userPhotoUri: String?,
         userPhotoThumb: String?,
         userPhotoUploadStatus: UploadStatus,
         userPhotoIndex: Int) {
        self.photoId = photoId
        self.photoUserId = photoUserId
        self.userPhotoUri = userPhotoUri
        self.userPhotoThumb = userPhotoThumb
        self.userPhotoUploadStatus = userPhotoUploadStatus
        self.userPhotoIndex = userPhotoIndex
    }
}

extension UserPhoto: RowConvertible, Persistable {
    public static var databaseTableName = CacheTables.userPhotosTable
}

extension UserPhoto {
    init(_ userId: UserModelId, _ userPhotoModel: UserPhotoModel, index: Int? = nil) {
        self.init(photoId: userPhotoModel.key.value,
                photoUserId: userId.value,
                userPhotoUri: userPhotoModel.uri,
                userPhotoThumb: userPhotoModel.thumb,
                userPhotoUploadStatus: userPhotoModel.uploadStatus,
                userPhotoIndex: index ?? userPhotoModel.index)

    }
}

public enum UploadStatus: String, Codable {
    case finished = "FINISHED"
    case pending = "PENDING"
    case notStarted = "NOT_STARTED"
}



