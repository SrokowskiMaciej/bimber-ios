//
//  UserProfile.swift
//  Bimber
//
//  Created by Maciek on 04.12.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import GRDB

struct UserProfile: Equatable, Decodable, RowConvertible {
    let user: User
    let photo: UserPhoto
}