//
// Created by Maciek on 13/02/2018.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher


public struct DefaultValues {
    public static let undefinedKey = "undefined"
    //Set on a server side
    public static let unknownUserName = "Unknown user"

    public func defaultPhoto(ofUser uId: UserId) -> UserPhoto {
        return UserPhoto(photoId: DefaultValues.undefinedKey, photoUserId: uId, userPhotoUri: nil, userPhotoThumb: nil, userPhotoUploadStatus: .finished, userPhotoIndex: 0)
    }

    public func defaultUser(uId: UserId) -> User {
        return User(uId: uId, displayName: "unknown_user_name".localized, fullName: "unknown_user_name".localized, about: "", gender: .male, friends: 0, parties: 0, age: 25, existing: false)
    }

    public func deletedUser(uId: UserId) -> User {
        return User(uId: uId, displayName: "deleted_user_name".localized, fullName: "deleted_user_name".localized, about: "", gender: .male, friends: 0, parties: 0, age: 25, existing: false)
    }
}

extension UserPhoto: DefaultableImage {
    public var getUrl: URL? {
        if let existingUri = self.userPhotoUri {
            return URL(string: existingUri)
        } else {
            return nil
        }
    }
    public var getDefaultImage: UIImage {
        return UIImage(named: "ic_default_profile")!
    }
}


public protocol DefaultableImage {
    var getUrl: URL? { get }
    var getDefaultImage: UIImage { get }
}

public extension UIImageView {
    public func showDefaultableImage(_ image: DefaultableImage) {
        if let originalImage = image.getUrl {
            self.kf.setImage(with: originalImage, placeholder: image.getDefaultImage)
        } else {
            self.image = image.getDefaultImage
        }
    }

}

