//
// Created by Maciej Srokowski on 4/19/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct PersonEvaluationModel: FirebaseValue {
    typealias KeyType = UserModelId
    let key: UserModelId
    let dialogId: ChatModelId?
    let status: PersonEvaluationStatus

    public init(snapshot: DataSnapshot) throws {
        self.key = UserModelId(key: snapshot.key)
        guard let val = snapshot.value as? NSDictionary,
              let evaluationStatusValue = val["status"] as? String,
              let status = PersonEvaluationStatus(rawValue: evaluationStatusValue) else {
            throw CustomError.missingFieldError("Missing some fields in user evaluation model")
        }

        if let dialogIdValue = val["dialogId"] as? String {
            self.dialogId = ChatModelId(key: dialogIdValue)
        } else {
            self.dialogId = nil
        }
        self.status = status
    }

    public func toFirebaseValue() -> [AnyHashable: Any] {
        return ["dialogId": self.dialogId?.value,
                "status": self.status.rawValue]
    }
}
