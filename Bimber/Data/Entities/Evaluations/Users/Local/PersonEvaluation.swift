//
// Created by Maciej Srokowski on 4/19/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation

struct PersonEvaluation {
    let evaluatingPersonId: UserId
    let evaluatedPersonId: UserId
    let dialogId: ChatId?
    let status: PersonEvaluationStatus
}

extension PersonEvaluation {
    init(evaluatingUser: UserId, evaluatedUser: UserId, _ personEvaluationModel: PersonEvaluationModel) {
        self.init(evaluatingPersonId: evaluatingUser,
                evaluatedPersonId: evaluatedUser,
                dialogId: personEvaluationModel.dialogId?.value,
                status: personEvaluationModel.status)
    }
}

enum PersonEvaluationStatus: String, Codable {
    //NONE status is not settable on client side. It means that user is available for discovery as same as user without evaluation
    // status
    case none = "NONE"
    case liked = "LIKED"
    case disliked = "DISLIKED"
    //MATCHED status is not settable on client side. Backend takes care of that
    case matched = "MATCHED"
}

