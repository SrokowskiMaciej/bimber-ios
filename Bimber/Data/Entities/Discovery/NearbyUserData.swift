//
// Created by Maciej Srokowski on 4/16/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation

struct NearbyUserData: Equatable {
    let user: UserProfile
    let userLocation: UserLocation?
    let referenceLocation: UserLocation?
    let background: CardBackground

    public init(user: UserProfile,
                userLocation: UserLocation?,
                referenceLocation: UserLocation?) {
        self.user = user
        self.userLocation = userLocation
        self.referenceLocation = referenceLocation
        self.background = CardBackground.randomCardBackground()
    }
}
