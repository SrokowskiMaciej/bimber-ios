//
// Created by Maciej Srokowski on 5/10/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation

enum Environment {
    case staging
    case production

    func baseURL() -> String {
        switch self {
        case .staging: return "https://bimber-staging.appspot.com/"
        case .production: return "https://bimber-a8ebe.appspot.com/"
        }
    }
}

struct Configuration {
    let environment: Environment

    init() {
        if let configuration = Bundle.main.object(forInfoDictionaryKey: "CONFIGURATION") as? String {
            if configuration.contains("Staging") {
                environment = .staging
            } else {
                environment = .production
            }
        } else {
            environment = .production
        }
    }
}
