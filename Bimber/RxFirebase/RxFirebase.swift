//
//  RxFirebase.swift
//  Bimber
//
//  Created by Maciek on 03.09.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import RxSwift
import FirebaseDatabase

public extension DatabaseQuery {

    func rxObserve(eventType: DataEventType) -> Observable<DataSnapshot> {
        return Observable.create { observer in
            let handle = self.observe(eventType,
                    with: { snapshot in observer.onNext(snapshot) },
                    withCancel: { error in observer.onError(error) })

            return Disposables.create {
                self.removeObserver(withHandle: handle)
            }
        }
    }

    func rxObserveWithSiblingKey(eventType: DataEventType) -> Observable<(DataSnapshot, String?)> {
        return Observable.create { observer in
            let handle = self.observe(eventType,
                    andPreviousSiblingKeyWith: { (snapshot, previousSiblingKey) in observer.onNext((snapshot, previousSiblingKey)) },
                    withCancel: { error in observer.onError(error) })

            return Disposables.create {
                self.removeObserver(withHandle: handle)
            }

        }
    }

    func rxObserveSingle(eventType: DataEventType) -> Observable<DataSnapshot> {
        return Observable.create { observer in
            self.observeSingleEvent(of: eventType,
                    with: { snapshot in
                        observer.onNext(snapshot)
                        observer.onCompleted()
                    },
                    withCancel: { error in observer.onError(error) })
            return Disposables.create()
        }
    }

    func rxObserveChildren() -> Observable<FirebaseChildEvent> {
        return Observable<FirebaseChildEvent>.create { observer in
            let addedHandle = self
                    .observe(.childAdded,
                    andPreviousSiblingKeyWith: { (snapshot, previousSiblingKey) in
                        observer.onNext(FirebaseChildEvent.childAdded(snapshot, previousSiblingKey))
                    }, withCancel: { error in observer.onError(error) })

            let removedHandle = self
                    .observe(.childRemoved,
                    andPreviousSiblingKeyWith: { (snapshot, previousSiblingKey) in
                        observer.onNext(FirebaseChildEvent.childRemoved(snapshot, previousSiblingKey))
                    },
                    withCancel: { error in observer.onError(error) })

            let changedHandle = self
                    .observe(.childChanged,
                    andPreviousSiblingKeyWith: { (snapshot, previousSiblingKey) in
                        observer.onNext(FirebaseChildEvent.childChanged(snapshot, previousSiblingKey))
                    },
                    withCancel: { error in observer.onError(error) })

            let movedHandle = self
                    .observe(.childMoved,
                    andPreviousSiblingKeyWith: { (snapshot, previousSiblingKey) in
                        observer.onNext(FirebaseChildEvent.childMoved(snapshot, previousSiblingKey))
                    },
                    withCancel: { error in observer.onError(error) })

            self
                    .observeSingleEvent(of: .value,
                    with: { snapshot in observer.onNext(FirebaseChildEvent.queryComplete(snapshot)) },
                    withCancel: { error in observer.onError(error) })

            return Disposables.create {
                self.removeObserver(withHandle: addedHandle)
                self.removeObserver(withHandle: removedHandle)
                self.removeObserver(withHandle: changedHandle)
                self.removeObserver(withHandle: movedHandle)
            }
        }
    }
}

public extension Observable where Element == DataSnapshot {
    public func mapChildren<T>(_ mapping: @escaping (DataSnapshot) throws -> T) -> Observable<Array<T>> {
        return self.flatMap({ dataSnapshot in
            Observable.from(dataSnapshot.children.allObjects as! Array<DataSnapshot>)
                    .map(mapping)
                    .toArray()
        })
    }

    public func mapValue<T>(_ mapping: @escaping (DataSnapshot) throws -> T) -> Observable<Array<T>> {
        return self.map { snapshot in
            if (snapshot.exists()) {
                return try [mapping(snapshot)]
            } else {
                return []
            }
        }
    }
}


public extension DatabaseReference {
    func rxSet(value: Any?) -> Completable {
        return Completable.create { completable in

            self.setValue(value,
                    withCompletionBlock: { (error, reference) in
                        if let error = error {
                            completable(.error(error))
                        } else {
                            completable(.completed)

                        }

                    })
            return Disposables.create()

        }
    }

    func rxSet(values: [AnyHashable: Any]) -> Completable {
        return Completable.create { completable in
            self.updateChildValues(values,
                    withCompletionBlock: { (error, reference) in
                        if let error = error {
                            completable(.error(error))
                        } else {
                            completable(.completed)

                        }

                    })
            return Disposables.create()

        }
    }

    func rxRemove() -> Completable {
        return Completable.create { completable in
            self.removeValue(completionBlock: { (error, reference) in
                if let error = error {
                    completable(.error(error))
                } else {
                    completable(.completed)
                }
            })
            return Disposables.create()
        }
    }

}

public enum FirebaseChildEvent: Equatable {
    case childAdded(DataSnapshot, String?)
    case childRemoved(DataSnapshot, String?)
    case childChanged(DataSnapshot, String?)
    case childMoved(DataSnapshot, String?)
    case queryComplete(DataSnapshot)

    public static func ==(lhs: FirebaseChildEvent, rhs: FirebaseChildEvent) -> Bool {
        switch (lhs, rhs) {
        case (.childAdded, .childAdded):
            return true
        case (.childRemoved, .childRemoved):
            return true
        case (.childChanged, .childChanged):
            return true
        case (.childMoved, .childMoved):
            return true
        case (.queryComplete, .queryComplete):
            return true
        default:
            return false
        }
    }

    public func snapshot() -> DataSnapshot {
        switch self {
        case .childAdded(let snapshot, _):
            return snapshot
        case .childRemoved(let snapshot, _):
            return snapshot
        case .childMoved(let snapshot, _):
            return snapshot
        case .childChanged(let snapshot, _):
            return snapshot
        case .queryComplete(let snapshot):
            return snapshot
        }
    }
}


