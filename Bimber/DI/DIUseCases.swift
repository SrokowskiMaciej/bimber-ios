//
// Created by Maciej Srokowski on 02/09/2018.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import Swinject

extension Container {

    func registerUseCases() {
        //USER DISCOVERY
        register(GetMatchStatusUseCase.self, factory: { r in
            GetMatchStatusUseCase(personEvaluationRepository: r.resolve(PersonEvaluationRepository.self)!)
        })
        register(GetDeeplinkUserUseCase.self, factory: { r in
            GetDeeplinkUserUseCase(userDeeplinkRepository: r.resolve(UserDeeplinkRepository.self)!,
                                   userProfileRepository: r.resolve(UserProfileRepository.self)!,
                                   getMatchChatIdUseCase: r.resolve(GetMatchStatusUseCase.self)!)
        })
        register(GetNearbyDiscoverableUsersUseCase.self, factory: { r in
            GetNearbyDiscoverableUsersUseCase(defaultValues: r.resolve(DefaultValues.self)!,
                                              userProfileRepository: r.resolve(UserProfileRepository.self)!,
                                              userLocationRepository: r.resolve(UsersLocationRepository.self)!,
                                              nearbyDiscoverableUsersRepository: r.resolve(NearbyDiscoverableUsersRepository.self)!,
                                              backgroundSchedulerProvider: r.resolve(BackgroundSchedulerProvider.self)!)
        })
        register(GetRecentlyRejectedUserUseCase.self, factory: { r in
            GetRecentlyRejectedUserUseCase(recentlyRejectedUserRepository: r.resolve(RecentlyRejectedUserRepository.self)!)
        })
        register(EvaluateUserUseCase.self, factory: { r in
            EvaluateUserUseCase(personEvaluationRepository: r.resolve(PersonEvaluationRepository.self)!,
                                userProfileRepository: r.resolve(UserProfileRepository.self)!,
                                nearbyDiscoverableUsersRepository: r.resolve(NearbyDiscoverableUsersRepository.self)!)
        })
    }
}
