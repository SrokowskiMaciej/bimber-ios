//
// Created by Maciej Srokowski on 02/09/2018.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import Swinject

extension Container {

    func registerViewModels() {
        register(EditProfileViewModel.self, factory: { r in
            EditProfileViewModel(authService: r.resolve(AuthService.self)!,
                                 userRepository: r.resolve(UsersRepository.self)!,
                                 userPhotoRepository: r.resolve(UserPhotosRepository.self)!,
                                 favouritePlacesRepository: r.resolve(FavouritePlacesRepository.self)!,
                                 favouriteTreatsRepository: r.resolve(FavouriteTreatsRepository.self)!)
        })

        register(DiscoverPeopleViewModel.self, factory: { r in
            DiscoverPeopleViewModel(
                    backgroundSchedulerProvider: r.resolve(BackgroundSchedulerProvider.self)!,
                    authService: r.resolve(AuthService.self)!,
                    getNearbyDiscoverableUsersUseCase: r.resolve(GetNearbyDiscoverableUsersUseCase.self)!,
                    getDeeplinkUserUseCase: r.resolve(GetDeeplinkUserUseCase.self)!,
                    getRecentlyRejectedUserUseCase: r.resolve(GetRecentlyRejectedUserUseCase.self)!,
                    evaluateUserUseCase: r.resolve(EvaluateUserUseCase.self)!)
        })
    }
}