//
// Created by Maciej Srokowski on 02/09/2018.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import Swinject
import GRDB
import Firebase
import FirebaseAuthUI

extension Container {

    func registerServices() {
        register(Configuration.self) { _ in
            Configuration()
        }
        register(Database.self) { _ in
            Database.database()
        }
        register(DatabaseQueue.self) { r in
            let databaseURL = try! FileManager.default
                    .url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                    .appendingPathComponent("db.sqlite")
            let databaseConnection = try! DatabaseQueue(path: databaseURL.path)
            try! createCacheTables(databaseConnection)
            return databaseConnection
        }
        //TODO Remove auth
        register(Auth.self) { _ in
            Auth.auth()
        }
        register(AuthService.self) { _ in
            AuthService()
        }
        register(DefaultValues.self) { r in
            DefaultValues()
        }
        register(BackgroundSchedulerProvider.self) { r in
            BackgroundSchedulerProvider()
        }
    }
}

