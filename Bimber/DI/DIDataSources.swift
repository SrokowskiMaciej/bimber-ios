//
// Created by Maciej Srokowski on 02/09/2018.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import Swinject
import GRDB
import FirebaseDatabase

extension Container {

    func registerDataSources() {
        //USERS
        register(UsersNetworkSource.self) { r in
            UsersNetworkSource(db: r.resolve(Database.self)!)
        }
        register(UsersLocalSource.self) { r in
            UsersLocalSource(db: r.resolve(DatabaseQueue.self)!)
        }
        register(UsersRepository.self) { r in
            UsersRepository(
                    defaultValues: r.resolve(DefaultValues.self)!,
                    localSource: r.resolve(UsersLocalSource.self)!,
                    networkSource: r.resolve(UsersNetworkSource.self)!,
                    backgroundSchedulerProvider: r.resolve(BackgroundSchedulerProvider.self)!)
        }
        //USER LOCATION
        register(UsersLocationNetworkSource.self) { r in
            UsersLocationNetworkSource(db: r.resolve(Database.self)!)
        }
        register(UsersLocationLocalSource.self) { r in
            UsersLocationLocalSource(db: r.resolve(DatabaseQueue.self)!)
        }
        register(UsersLocationRepository.self) { r in
            UsersLocationRepository(localSource: r.resolve(UsersLocationLocalSource.self)!,
                                    networkSource: r.resolve(UsersLocationNetworkSource.self)!,
                                    backgroundSchedulerProvider: r.resolve(BackgroundSchedulerProvider.self)!)
        }
        //USER PHOTOS
        register(UserPhotoNetworkSource.self) { r in
            UserPhotoNetworkSource(db: r.resolve(Database.self)!)
        }
        register(UserPhotosLocalSource.self) { r in
            UserPhotosLocalSource(db: r.resolve(DatabaseQueue.self)!)
        }
        register(UserPhotosRepository.self) { r in
            UserPhotosRepository(defaultValues: r.resolve(DefaultValues.self)!,
                                 localSource: r.resolve(UserPhotosLocalSource.self)!,
                                 networkSource: r.resolve(UserPhotoNetworkSource.self)!,
                                 backgroundSchedulerProvider: r.resolve(BackgroundSchedulerProvider.self)!)
        }
        //USER FAVOURITE PLACES
        register(FavouritePlacesNetworkSource.self) { r in
            FavouritePlacesNetworkSource(db: r.resolve(Database.self)!)
        }
        register(FavouritePlacesLocalSource.self) { r in
            FavouritePlacesLocalSource(db: r.resolve(DatabaseQueue.self)!)
        }
        register(FavouritePlacesRepository.self) { r in
            FavouritePlacesRepository(localSource: r.resolve(FavouritePlacesLocalSource.self)!,
                                      networkSource: r.resolve(FavouritePlacesNetworkSource.self)!,
                                      backgroundSchedulerProvider: r.resolve(BackgroundSchedulerProvider.self)!)
        }
        //USER FAVOURITE TREATS
        register(FavouriteTreatsNetworkSource.self) { r in
            FavouriteTreatsNetworkSource(db: r.resolve(Database.self)!)
        }
        register(FavouriteTreatsLocalSource.self) { r in
            FavouriteTreatsLocalSource(db: r.resolve(DatabaseQueue.self)!)
        }
        register(FavouriteTreatsRepository.self) { r in
            FavouriteTreatsRepository(localSource: r.resolve(FavouriteTreatsLocalSource.self)!,
                                      networkSource: r.resolve(FavouriteTreatsNetworkSource.self)!,
                                      backgroundSchedulerProvider: r.resolve(BackgroundSchedulerProvider.self)!)
        }

        //USERS PROFILES
        register(UserProfileLocalSource.self) { r in
            UserProfileLocalSource(db: r.resolve(DatabaseQueue.self)!,
                                   backgroundSchedulerProvider: r.resolve(BackgroundSchedulerProvider.self)!)
        }
        register(UserProfileRepository.self) { r in
            UserProfileRepository(usersRepository: r.resolve(UsersRepository.self)!,
                                  photosRepository: r.resolve(UserPhotosRepository.self)!,
                                  localSource: r.resolve(UserProfileLocalSource.self)!)
        }
        //EVALUATIONS
        register(PersonEvaluationsNetworkSource.self) { r in
            PersonEvaluationsNetworkSource(db: r.resolve(Database.self)!)
        }
        register(PersonEvaluationRepository.self) { r in
            PersonEvaluationRepository(networkSource: r.resolve(PersonEvaluationsNetworkSource.self)!)
        }

        //USER DISCOVERY
        register(UserDeeplinkRepository.self) { r in
            UserDeeplinkRepository()
        }
        register(RecentlyRejectedUserRepository.self) { r in
            RecentlyRejectedUserRepository()
        }
        register(NearbyDiscoverableUsersNetworkSource.self) { r in
            NearbyDiscoverableUsersNetworkSource(configuration: r.resolve(Configuration.self)!)
        }
        register(NearbyDiscoverableUsersInMemorySource.self) { r in
            NearbyDiscoverableUsersInMemorySource()
        }
        register(NearbyDiscoverableUsersRepository.self) { r in
            NearbyDiscoverableUsersRepository(networkSource: r.resolve(NearbyDiscoverableUsersNetworkSource.self)!,
                                              inMemorySource: r.resolve(NearbyDiscoverableUsersInMemorySource.self)!)
        }
    }
}
