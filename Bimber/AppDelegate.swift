//
//  AppDelegate.swift
//  Bimber
//
//  Created by Maciek on 14.08.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import UIKit
import GRDB
import Firebase
import FirebaseAuthUI
import GoogleMaps
import GooglePlaces
import Swinject
import SwinjectStoryboard
import CocoaLumberjack

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    public static let container: Container = {
        let container = Container()
        container.registerServices()
        container.registerDataSources()
        container.registerUseCases()
        container.registerViewModels()
        container.storyboardInitCompleted(LaunchController.self) { resolver, controller in
            controller.auth = resolver.resolve(Auth.self)
        }
        container.storyboardInitCompleted(LocationPickerController.self) { resolver, controller in
            controller.locationRepository = resolver.resolve(UsersLocationNetworkSource.self)
            controller.auth = resolver.resolve(Auth.self)
        }
        return container
    }()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        GMSServices.provideAPIKey("AIzaSyCYOz6klsO5Cy_SvGJh_ZmjvXCqjdMUev0")
        GMSPlacesClient.provideAPIKey("AIzaSyCYOz6klsO5Cy_SvGJh_ZmjvXCqjdMUev0")

        DDLog.add(DDTTYLogger.sharedInstance) // TTY = Xcode console

        let window = UIWindow(frame: UIScreen.main.bounds)
        window.makeKeyAndVisible()
        self.window = window


        let storyboard = SwinjectStoryboard.create(name: "Main", bundle: nil, container: AppDelegate.container)
        window.rootViewController = storyboard.instantiateInitialViewController()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any]) -> Bool {
        let sourceApplication = options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String?
        return self.handleOpenUrl(url, sourceApplication: sourceApplication)
    }

    @available(iOS 8.0, *)
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return self.handleOpenUrl(url, sourceApplication: sourceApplication)
    }

    func handleOpenUrl(_ url: URL, sourceApplication: String?) -> Bool {
        if FUIAuth.defaultAuthUI()?.handleOpen(url, sourceApplication: sourceApplication) ?? false {
            return true
        }
        // other URL handling goes here.
        return false
    }
}

