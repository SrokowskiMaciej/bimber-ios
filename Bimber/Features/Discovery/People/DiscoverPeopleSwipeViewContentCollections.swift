//
// Created by Maciej Srokowski on 5/5/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa
import RxDataSources
import CoreLocation

extension NearbyUserData: IdentifiableType {
    public typealias Identity = UserId
    public var identity: UserId {
        return self.user.user.uId
    }
}

struct NearbyUsersSection {
    let items: [NearbyUserData]
}

extension NearbyUsersSection: AnimatableSectionModelType {
    typealias Identity = String
    typealias Item = NearbyUserData

    var identity: String {
        return "nearbyUsersSection"
    }

    init(original: NearbyUsersSection, items: [NearbyUserData]) {
        self.items = items
    }
}

public class NearbyUserItemCell: UICollectionViewCell {

    static let identifier = "NearbyUserItemCellIdentifier"

    lazy var profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 54
        imageView.clipsToBounds = true
        return imageView
    }()

    lazy var profileNameLabel: UILabel = {
        let label = UILabel()
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        return label
    }()

    lazy var aboutLabel: UILabel = {
        let label = UILabel()
        return label
    }()

    lazy var locationIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = R.image.ic_place_white_24pt()
        imageView.tintColor = BimberColors.colorWhite
        return imageView
    }()

    lazy var friendsLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        return label
    }()

    lazy var friendsIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = R.image.ic_person_white_24pt()
        imageView.tintColor = BimberColors.colorPrimary
        return imageView
    }()

    lazy var partiesLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        return label
    }()

    lazy var partiesIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = R.image.ic_group_white_24pt()
        imageView.tintColor = BimberColors.colorPrimary
        return imageView
    }()

    lazy var locationPlaceLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textColor = BimberColors.colorWhite
        return label
    }()

    lazy var locationDistanceLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textColor = BimberColors.colorWhite
        return label
    }()

    lazy var locationLabelsBackground: UIView = {
        let background = UIView()
        background.backgroundColor = BimberColors.colorPrimary
        return background
    }()

    lazy var positiveMarkLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textColor = BimberColors.colorPositiveAction
        label.text = R.string.localizable.view_discover_people_like()
        label.alpha = 0.0
        label.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 4)
        return label
    }()

    lazy var negativeMarkLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textColor = BimberColors.colorNegativeAction
        label.text = R.string.localizable.view_discover_people_dislike()
        label.alpha = 0.0
        label.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 4)
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        contentView.addSubview(profileImageView)
        contentView.addSubview(profileNameLabel)
        contentView.addSubview(aboutLabel)
        contentView.addSubview(friendsIcon)
        contentView.addSubview(friendsLabel)
        contentView.addSubview(partiesIcon)
        contentView.addSubview(partiesLabel)
        contentView.addSubview(locationLabelsBackground)
        contentView.addSubview(locationIcon)
        contentView.addSubview(locationPlaceLabel)
        contentView.addSubview(locationDistanceLabel)
        contentView.addSubview(positiveMarkLabel)
        contentView.addSubview(negativeMarkLabel)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 4.0
        self.autoresizesSubviews = true
    }

    override public func layoutSubviews() {
        profileImageView.snp.makeConstraints { make in
            make.width.height.equalTo(108)
            make.left.top.equalTo(self).offset(12)
        }
        profileNameLabel.snp.makeConstraints { make in
            make.left.equalTo(self.profileImageView.snp.right)
            make.top.right.equalToSuperview()
            make.bottom.equalTo(self.profileImageView.snp.bottom)
        }
        aboutLabel.snp.makeConstraints { make in
            make.top.equalTo(self.profileImageView.snp.bottom)
            make.bottom.equalTo(self.friendsIcon.snp.top)
            make.centerX.equalToSuperview()
        }
        locationLabelsBackground.snp.makeConstraints { make in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(self.locationPlaceLabel.snp.top)
        }
        locationIcon.snp.makeConstraints { make in
            make.left.equalToSuperview()
            make.centerY.equalTo(self.locationLabelsBackground)
            make.width.height.equalTo(24)
        }
        locationDistanceLabel.snp.makeConstraints { make in
            make.left.equalTo(self.locationIcon.snp.right)
            make.right.bottom.equalToSuperview()
        }
        locationPlaceLabel.snp.makeConstraints { make in
            make.left.equalTo(self.locationIcon.snp.right)
            make.right.equalToSuperview()
            make.bottom.equalTo(self.locationDistanceLabel.snp.top)
        }
        friendsLabel.snp.makeConstraints { make in
            make.left.equalToSuperview()
            make.bottom.equalTo(self.locationLabelsBackground.snp.top)
        }
        friendsIcon.snp.makeConstraints { make in
            make.centerX.equalTo(self.friendsLabel)
            make.bottom.equalTo(self.friendsLabel.snp.top)
            make.width.height.equalTo(24)
        }
        partiesLabel.snp.makeConstraints { make in
            make.right.equalToSuperview()
            make.bottom.equalTo(self.locationLabelsBackground.snp.top)
        }
        partiesIcon.snp.makeConstraints { make in
            make.centerX.equalTo(self.partiesLabel)
            make.bottom.equalTo(self.partiesLabel.snp.top)
            make.width.height.equalTo(24)
        }
        positiveMarkLabel.snp.makeConstraints { make in
            make.left.top.equalToSuperview()
        }
        negativeMarkLabel.snp.makeConstraints { make in
            make.left.top.equalToSuperview()
        }
    }

    public override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        self.layer.zPosition = CGFloat(layoutAttributes.zIndex)
    }

    func setupFor(nearbyUser: NearbyUserData) {
        profileNameLabel.text = nearbyUser.user.user.displayName
        aboutLabel.text = nearbyUser.user.user.about
        profileImageView.showDefaultableImage(nearbyUser.user.photo)
        profileImageView.layer.cornerRadius = 64

        friendsLabel.text = R.string.localizable.profile_friends_count(x: nearbyUser.user.user.friends)
        partiesLabel.text = R.string.localizable.profile_parties_count(x: nearbyUser.user.user.parties)

        if let originLocation = nearbyUser.userLocation,
           let referenceLocation = nearbyUser.referenceLocation {
            locationPlaceLabel.text = originLocation.userLocationName
            let distance: CLLocationDistance = (CLLocation(latitude: originLocation.userLocationLatitude, longitude: originLocation.userLocationLongitude)
                    .distance(from: CLLocation(latitude: referenceLocation.userLocationLatitude, longitude: referenceLocation.userLocationLongitude)) / 1000)
            locationDistanceLabel.text = distance > 1 ? String(distance.rounded()) : "1"
        } else {
            locationPlaceLabel.text = R.string.localizable.view_discover_card_unknown_location()
        }
        self.backgroundColor = nearbyUser.background.backgroundColor()
    }

    func show(progress: CGFloat, in direction: SwipeDirection) {
        switch direction {
        case .left:
            positiveMarkLabel.alpha = progress
        case .top:
            ()
        case .right:
            negativeMarkLabel.alpha = progress
        case .bottom:
            ()
        }
    }

}