//
// Created by Maciej Srokowski on 01/09/2018.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import UIKit

class MatchView: UIView {

    private lazy var title: UILabel = {
        let label = UILabel()
        label.text = R.string.localizable.view_discover_people_match_dialog_title()
        return label
    }()

    private lazy var content: UILabel = {
        let label = UILabel()
        return label
    }()

    private lazy var currentUserImageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()

    private lazy var matchedUserImageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()

    private lazy var chatButton: UIButton = {
        let button = UIButton()
        button.setTitle(R.string.localizable.view_discover_people_match_dialog_go_to_chat_button_text(), for: .normal)
        return button
    }()

    private lazy var dismissButton: UIButton = {
        let button = UIButton()
        button.setTitle(R.string.localizable.dialog_ok_button_text(), for: .normal)
        return button
    }()

    override func layoutSubviews() {
        super.layoutSubviews()
        self.title.snp.makeConstraints { make in
            make.left.top.right.equalToSuperview()
        }
        self.content.snp.makeConstraints { make in
            make.top.equalTo(self.title.snp.bottom)
            make.left.right.equalToSuperview()
        }

    }

    func render(for viewState: MatchViewModel.ViewState) {
        if case .loaded(let userProfile) = viewState.matchedUserData {
            self.content.text = R.string.localizable.view_discover_match_dialog_content_text(userProfile.user.displayName)
        }

        switch viewState.chat {
        case .notAvailable:
            self.chatButton.isHidden = true
        case .available(let chatId):
            self.chatButton.isHidden = false
        }
    }
}
