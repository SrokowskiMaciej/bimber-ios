//
// Created by Maciej Srokowski on 01/09/2018.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import RxSwift

struct MatchViewModel {

    let authService: AuthService
    let userProfileRepository: UserProfileRepository
    let getMatchStatusUseCase: GetMatchStatusUseCase

    private let eventsSubject: PublishSubject<Event> = PublishSubject()

    // MARK: PUBLIC

    func handle(event: Event) {
        self.eventsSubject.onNext(event)

    }

    func state() -> Observable<ViewState> {
        return self.reduceViewState()
    }

    // MARK: EVENTS

    private func events() -> Observable<Event> {
        return eventsSubject
    }

    // MARK: STORES

    private func authStore() -> Observable<UserId> {
        return authService.authenticated()
                .filter({ id in id != nil })
                .map({ id in id! })
                .share(replay: 1)
    }

    // MARK: ACTIONS

    private func currentUserProfileActions() -> Observable<Action> {
        return self.authStore().flatMapLatest { currentUserId -> Observable<Action> in
            self.userProfileRepository.onProfileData(ofUser: currentUserId)
                    .map({ (userProfile: UserProfile) -> Action in .currentUserData(userProfile) })
        }
    }

    private func matchedUserProfileActions() -> Observable<Action> {
        return self.events()
                .flatMap { event -> Observable<Action> in
                    switch event {
                    case .initial(let matchedUserId):
                        return self.userProfileRepository.onProfileData(ofUser: matchedUserId)
                                .map({ (profile: UserProfile) -> Action in .matchedUserData(profile) })
                    default:
                        return Observable.empty()
                    }
                }
    }

    private func chatAvailableActions() -> Observable<Action> {
        typealias MatchStatus = GetMatchStatusUseCase.MatchStatus
        return Observable.combineLatest(self.authStore(), self.events(), resultSelector: { (currentUserId, event) ->
                        Observable<(UserId, UserId)> in
                    switch event {
                    case .initial(let matchedUserId):
                        return Observable.just((currentUserId, matchedUserId))
                    default:
                        return Observable.empty()
                    }
                })
                .flatMapLatest { usersObservable -> Observable<MatchStatus> in
                    usersObservable.flatMap { usersIds -> Observable<MatchStatus> in
                        let (currentUserId, matchedUserId) = usersIds
                        return self.getMatchStatusUseCase
                                .onMatchStatus(currentUserId: currentUserId, userToCheckId: matchedUserId)
                    }
                }
                .flatMap { matchStatus -> Observable<Action> in
                    switch matchStatus {
                    case .matched(let chatId):
                        return Observable.just(.chatAvailable(chatId: chatId))
                    default:
                        return Observable.empty()
                    }
                }
    }

    // MARK: REDUCER

    private func reduceViewState() -> Observable<ViewState> {
        return Observable.merge(currentUserProfileActions(),
                                matchedUserProfileActions(),
                                chatAvailableActions())
                .catchErrorJustReturn(.error)
                .scan(self.defaultViewState) { (previousState: ViewState, action: Action) in
                    switch action {
                    case .none:
                        return previousState
                    case .currentUserData(let userData):
                        return ViewState(from: previousState,
                                         currentUserData: .loaded(userData))
                    case .matchedUserData(let userData):
                        return ViewState(from: previousState,
                                         matchedUserData: .loaded(userData))
                    case .chatAvailable(let chatId):
                        return ViewState(from: previousState,
                                         chat: .available(chatId: chatId))
                    case .error:
                        return ViewState(from: previousState,
                                         showError: true)
                    }

                }
    }

    enum Event {
        case initial(matchedUserId: UserId)
        case openChat(String)
        case closeDialog
    }

    private enum Action {
        case none
        case currentUserData(UserProfile)
        case matchedUserData(UserProfile)
        case chatAvailable(chatId: String)
        case error
    }

    struct ViewState {
        enum Chat {
            case notAvailable
            case available(chatId: String)
        }

        enum LoadingUserProfile {
            case loading
            case loaded(UserProfile)
        }

        let currentUserData: LoadingUserProfile
        let matchedUserData: LoadingUserProfile
        let chat: Chat
        let showError: Bool
    }

    private let defaultViewState = ViewState(currentUserData: .loading,
                                             matchedUserData: .loading,
                                             chat: .notAvailable,
                                             showError: false)
}

extension MatchViewModel.ViewState {
    init(from model: MatchViewModel.ViewState,
         currentUserData: LoadingUserProfile? = nil,
         matchedUserData: LoadingUserProfile? = nil,
         chat: Chat? = nil,
         showError: Bool? = nil) {
        self.currentUserData = currentUserData ?? model.currentUserData
        self.matchedUserData = matchedUserData ?? model.matchedUserData
        self.chat = chat ?? model.chat
        self.showError = showError ?? model.showError
    }
}
