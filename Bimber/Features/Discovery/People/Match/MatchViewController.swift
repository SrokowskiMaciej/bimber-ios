//
// Created by Maciej Srokowski on 01/09/2018.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import UIKit

class MatchViewController: UIViewController {

    private let viewModel: MatchViewModel

    init(viewModel: MatchViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError()
    }


}
