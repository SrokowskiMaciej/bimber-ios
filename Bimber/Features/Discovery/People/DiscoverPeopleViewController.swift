//
// Created by Maciej Srokowski on 4/15/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxDataSources

class DiscoverPeopleViewController: UIViewController, DiscoverPeopleViewNearbyUserSwipedDelegate {

    private let viewModel: DiscoverPeopleViewModel!

    private let disposables = DisposeBag()

    init(viewModel: DiscoverPeopleViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError()
    }

    override func loadView() {
        self.view = DiscoverPeopleView()
    }

    private var contentView: DiscoverPeopleView {
        return self.view as! DiscoverPeopleView
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let viewState = viewModel.state().share(replay: 1)
        viewState.map({ state in [NearbyUsersSection(items: state.nearbyUsers)] })
                .bind(to: self.contentView.swipeView.rx.items(dataSource: self.contentView.collectionViewSource))
    }

    func onSwiped(user: NearbyUserData, inDirection: SwipeDirection) {
        switch inDirection {
        case .left:
            self.viewModel.handle(event: .dislike(user))
        case .right:
            self.viewModel.handle(event: .like(user))
        case .top, .bottom:
            ()
        }
    }
}

extension NearbyUserData: Hashable {
    public var hashValue: Int {
        return self.user.user.uId.hashValue
    }
}
