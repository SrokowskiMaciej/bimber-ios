//
// Created by Maciej Srokowski on 4/22/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import RxSwift

struct DiscoverPeopleViewModel {

    let backgroundSchedulerProvider: BackgroundSchedulerProvider
    let authService: AuthService
    let getNearbyDiscoverableUsersUseCase: GetNearbyDiscoverableUsersUseCase
    let getDeeplinkUserUseCase: GetDeeplinkUserUseCase
    let getRecentlyRejectedUserUseCase: GetRecentlyRejectedUserUseCase
    let evaluateUserUseCase: EvaluateUserUseCase

    private let eventsSubject: PublishSubject<Event> = PublishSubject()

    // MARK: PUBLIC

    func handle(event: Event) {
        self.eventsSubject.onNext(event)

    }

    func state() -> Observable<ViewState> {
        return self.reduceViewState()
    }

    // MARK: EVENTS

    private func events() -> Observable<Event> {
        return eventsSubject
    }

    // MARK: STORES

    fileprivate typealias NearbyUsersResult = GetNearbyDiscoverableUsersUseCase.NearbyUsersResult
    fileprivate typealias DeeplinkUserResult = GetDeeplinkUserUseCase.DeeplinkUserResult
    fileprivate typealias RecentlyRejectedUserResult = GetRecentlyRejectedUserUseCase.RecentlyRejectedUserResult

    private func authStore() -> Observable<UserId> {
        return authService.authenticated()
                .filter({ id in id != nil })
                .map({ id in id! })
                .share(replay: 1)
    }

    private func nearbyUsersStore() -> Observable<NearbyUsersResult> {
        return self.authStore().flatMapLatest({ uId in self.getNearbyDiscoverableUsersUseCase.onDiscoverableUsers(currentUserId: uId) })
    }

    private func deeplinkStore() -> Observable<DeeplinkUserResult> {
        return self.authStore().flatMapLatest({ uId in self.getDeeplinkUserUseCase.onDeeplinkUser(currentUserId: uId) })
    }

    private func recentlyRejectedStore() -> Observable<RecentlyRejectedUserResult> {
        return self.authStore().flatMapLatest({ uId in self.getRecentlyRejectedUserUseCase.onRecentlyRejectedUser() })
    }

    // MARK: ACTIONS

    private func likedActions() -> Observable<Action> {
        return self.authStore().flatMapLatest { userId in
            self.events().flatMap { event -> Observable<Action> in
                switch event {
                case .like(let likedUser):
                    return self.evaluateUserUseCase
                            .likeUser(currentUserId: userId, likedUserId: likedUser.user.user.uId)
                            .asObservable()
                            .flatMap { matchData -> Observable<Action> in
                                switch matchData {
                                case .matched(let userId):
                                    return Observable.just(Action.matched(userId))
                                case .notMatched(_):
                                    return Observable.empty()
                                }
                            }
                default:
                    return Observable.empty()
                }
            }
        }
    }

    private func dislikedActions() -> Observable<Action> {
        return self.authStore().flatMapLatest { userId in
            self.events()
                    .flatMap { event -> Observable<Action> in
                        switch event {
                        case .like(let likedUser):
                            return self.evaluateUserUseCase
                                    .dislikeUser(currentUserId: userId, dislikedUserId: likedUser.user.user.uId)
                                    .asObservable()
                                    .flatMap({ data in return Observable.empty() })
                        default:
                            return Observable.empty()
                        }
                    }
        }
    }

    private func nearbyUsersActions() -> Observable<Action> {
        return Observable.combineLatest(self.nearbyUsersStore(), self.deeplinkStore(), self.recentlyRejectedStore()) {
            (nearbyUsersResult: NearbyUsersResult,
             deeplinkResult: DeeplinkUserResult,
             recentlyRejectedResult: RecentlyRejectedUserResult) -> Action in
            // Don't show users when waiting for a deeplink
            if case DeeplinkUserResult.loadingDeepLinkUserResult = deeplinkResult {
                return .nearbyUsers([])
            }
            // Don't show any users with no location chosen
            if case NearbyUsersResult.noLocationChosen = nearbyUsersResult {
                return .nearbyUsers([])
            }

            let deeplinkUsers: [NearbyUserData] = {
                switch deeplinkResult {
                case .successDeepLinkUserResult(let deeplinkUser):
                    return [deeplinkUser]
                default:
                    return []
                }
            }()

            let rejectedUsers: [NearbyUserData] = {
                switch recentlyRejectedResult {
                case .hasRejectedUserResult(let rejectedUser):
                    return [rejectedUser]
                case .hasRevertedUserResult(let revertedUser):
                    return [revertedUser]
                case .noRecentlyRejectedUserResult:
                    return []
                }
            }()

            let nearbyUsers: [NearbyUserData] = {
                switch nearbyUsersResult {
                case .loading(let remainingUsers):
                    return remainingUsers
                case .success(let nearbyUsers):
                    return nearbyUsers
                case .noMoreUsersInTheArea(let remainingUsers):
                    return remainingUsers
                case .error(let remainingUsers):
                    return remainingUsers
                case .noLocationChosen:
                    return []
                }
            }()
            return .nearbyUsers(Array(Set<NearbyUserData>(deeplinkUsers + rejectedUsers + nearbyUsers)))
        }
    }

    private func revertUserActions() -> Observable<Action> {
        return self.recentlyRejectedStore()
                .flatMap { recentlyRejectedUserResult -> Observable<Action> in
                    switch recentlyRejectedUserResult {
                    case .noRecentlyRejectedUserResult:
                        return Observable.empty()
                    case .hasRejectedUserResult(_):
                        return Observable.empty()
                    case .hasRevertedUserResult(let revertedUser):
                        return Observable.just(.revertUser(revertedUser))
                    }
                }
    }

    private func noMorePeopleActions() -> Observable<Action> {
        return self.nearbyUsersStore()
                .map { result -> Action in
                    switch result {
                    case .noMoreUsersInTheArea(_):
                        return Action.noMoreUsers(true)
                    default:
                        return Action.noMoreUsers(false)
                    }
                }
    }

    private func noLocationChosenActions() -> Observable<Action> {
        return self.nearbyUsersStore()
                .map { result -> Action in
                    switch result {
                    case .noLocationChosen:
                        return Action.noChosenLocation(true)
                    default:
                        return Action.noChosenLocation(false)

                    }
                }
    }

    private func deeplinkActions() -> Observable<Action> {
        return self.deeplinkStore()
                .map { (deeplinkUserResult: DeeplinkUserResult) -> Action.DeeplinkAction in
                    switch deeplinkUserResult {
                    case .noDeepLinkUserResult:
                        return .none
                    case .currentUserDeepLink:
                        return .deeplinkCurrentUser
                    case .loadingDeepLinkUserResult:
                        return .deeplinkLoading
                    case .alreadyMatchedUserDeepLink(let dialogId):
                        return .deeplinkAlreadyMatched(dialogId: dialogId)
                    case .successDeepLinkUserResult(_):
                        return .deeplinkSuccess
                    case .errorDeepLinkUserResult(_):
                        return .deeplinkError
                    }
                }
                .map({ action in .deeplink(action) })
    }


    // MARK: REDUCER

    private func reduceViewState() -> Observable<ViewState> {
        return Observable.merge(nearbyUsersActions(),
                                revertUserActions(),
                                noMorePeopleActions(),
                                noLocationChosenActions(),
                                deeplinkActions())
                .catchErrorJustReturn(Action.error)
                .scan(self.defaultView)
                { (previousState: ViewState, action: Action) -> ViewState in
                    switch action {
                    case .none:
                        return previousState
                    case .nearbyUsers(let users):
                        return ViewState(from: previousState,
                                         nearbyUsers: users)
                    case .revertUser(let user):
                        return ViewState(from: previousState,
                                         revertedUser: OneShot(user))
                    case .noMoreUsers(let noMore):
                        return ViewState(from: previousState,
                                         showNoMorePeople: noMore)
                    case .noChosenLocation(let noLocation):
                        return ViewState(from: previousState,
                                         showNoLocation: noLocation)
                    case .error:
                        return ViewState(from: previousState,
                                         showError: true)
                    case .deeplink(let deeplinkAction):
                        switch deeplinkAction {
                        case .none:
                            return ViewState(from: previousState,
                                             showProgress: false)
                        case .deeplinkLoading:
                            return ViewState(from: previousState,
                                             showProgress: true)
                        case .deeplinkCurrentUser:
                            return ViewState(from: previousState,
                                             showProgress: false,
                                             deeplinkResult: OneShot(.currentUser))
                        case .deeplinkAlreadyMatched(let dialogId):
                            return ViewState(from: previousState,
                                             showProgress: false,
                                             deeplinkResult: OneShot(.alreadyMatched(dialogId: dialogId)))
                        case .deeplinkSuccess:
                            return ViewState(from: previousState,
                                             showProgress: false)
                        case .deeplinkError:
                            return ViewState(from: previousState,
                                             showProgress: false,
                                             deeplinkResult: OneShot(.error))

                        }
                    case .matched(let userId):
                        return ViewState(from: previousState,
                                         matched: OneShot(userId))
                    }
                }
    }


    enum Event {
        case like(NearbyUserData)
        case dislike(NearbyUserData)
        case revertLast
        case deeplinkHandled
    }

    private enum Action {
        enum DeeplinkAction {
            case none
            case deeplinkLoading
            case deeplinkCurrentUser
            case deeplinkAlreadyMatched(dialogId: String)
            case deeplinkSuccess
            case deeplinkError
        }

        case none
        case nearbyUsers([NearbyUserData])
        case noMoreUsers(Bool)
        case noChosenLocation(Bool)
        case revertUser(NearbyUserData)
        case matched(UserId)
        case deeplink(DeeplinkAction)
        case error
    }

    struct ViewState: Equatable {
        enum DeeplinkResult: Equatable {
            case currentUser
            case alreadyMatched(dialogId: String)
            case error
        }

        let nearbyUsers: [NearbyUserData]
        let showProgress: Bool
        let showError: Bool
        let showNoLocation: Bool
        let showNoMorePeople: Bool
        let deeplinkResult: OneShot<DeeplinkResult>
        let revertedUser: OneShot<NearbyUserData>
        let matched: OneShot<UserId>
    }

    private let defaultView = ViewState(nearbyUsers: [],
                                        showProgress: false,
                                        showError: false,
                                        showNoLocation: false,
                                        showNoMorePeople: false,
                                        deeplinkResult: OneShot(),
                                        revertedUser: OneShot(),
                                        matched: OneShot())

}

extension DiscoverPeopleViewModel.ViewState {
    init(from model: DiscoverPeopleViewModel.ViewState,
         nearbyUsers: [NearbyUserData]? = nil,
         showProgress: Bool? = nil,
         showError: Bool? = nil,
         showNoLocation: Bool? = nil,
         showNoMorePeople: Bool? = nil,
         deeplinkResult: OneShot<DeeplinkResult>? = nil,
         revertedUser: OneShot<NearbyUserData>? = nil,
         matched: OneShot<UserId>? = nil) {
        self.nearbyUsers = nearbyUsers ?? model.nearbyUsers
        self.showProgress = showProgress ?? model.showProgress
        self.showError = showError ?? model.showError
        self.showNoLocation = showNoLocation ?? model.showNoLocation
        self.showNoMorePeople = showNoMorePeople ?? model.showNoMorePeople
        self.deeplinkResult = deeplinkResult ?? model.deeplinkResult
        self.revertedUser = revertedUser ?? model.revertedUser
        self.matched = matched ?? model.matched
    }
}

