//
// Created by Maciej Srokowski on 28/07/2018.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import RxDataSources

class DiscoverPeopleView: UIView, UICollectionViewDelegate, SwipeViewLayoutDelegate {

    private let layout = SwipeViewLayout()

    lazy var swipeView: UICollectionView = {
        layout.allowedDirections = [.left, .right]
        layout.delegate = self
        let view = UICollectionView(frame: self.frame, collectionViewLayout: layout)
        view.delegate = self
        view.register(NearbyUserItemCell.self, forCellWithReuseIdentifier: NearbyUserItemCell.identifier)
        return view
    }()

    lazy var collectionViewSource = {
        return RxCollectionViewSectionedAnimatedDataSource<NearbyUsersSection>(
                configureCell: { (sources, collectionView, indexPath, nearbyUser) -> UICollectionViewCell in
                    let cell: NearbyUserItemCell = collectionView.dequeueReusableCell(withReuseIdentifier: NearbyUserItemCell.identifier, for: indexPath) as! NearbyUserItemCell
                    cell.setupFor(nearbyUser: nearbyUser)
                    cell.clipsToBounds = true
                    return cell
                },
                configureSupplementaryView: { (sources, collectionView, identifier, indexPath) -> UICollectionReusableView in
                    return UICollectionReusableView()
                })
    }()

    var delegate: DiscoverPeopleViewNearbyUserSwipedDelegate?

    init() {
        super.init(frame: CGRect.zero)
        setup()
        makeConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
        makeConstraints()
    }

    private func setup() {
        addSubview(swipeView)
    }

    private func makeConstraints() {
        self.swipeView.snp.makeConstraints { maker in
            maker.left.top.right.bottom.equalToSuperview()
        }
    }

    func swipeTopUser(inDirection direction: SwipeDirection) {
        self.layout.swipe(inDirection: direction)
    }

    func revertSwipe(user: NearbyUserData) {

//        self.layout.reverseItem(at: <#T##IndexPath##Foundation.IndexPath#>)
//        let items = self.swipeView.rx.items(dataSource: collectionViewSource)
    }

    func on(swipeProgress: CGFloat, in direction: SwipeDirection, at indexPath: IndexPath) {
        (self.swipeView.cellForItem(at: indexPath) as? NearbyUserItemCell)?.show(progress: swipeProgress, in: direction)
    }

    func onItemSwiped(inDirection direction: SwipeDirection, at indexPath: IndexPath) {
        let swipedUserData = self.collectionViewSource[indexPath]
        self.delegate?.onSwiped(user: swipedUserData, inDirection: direction)
    }
}

protocol DiscoverPeopleViewNearbyUserSwipedDelegate {

    func onSwiped(user: NearbyUserData, inDirection: SwipeDirection)
}
