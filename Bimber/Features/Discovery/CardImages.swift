//
// Created by Maciej Srokowski on 5/18/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import UIKit

extension CardBackground {
    func backgroundColor() -> UIColor? {
        var image: UIImage?
        switch self {
        case .type1:
            image = R.image.img_card_1()
        case .type2:
            image = R.image.img_card_2()
        case .type3:
            image = R.image.img_card_3()
        case .type4:
            image = R.image.img_card_4()
        case .type5:
            image = R.image.img_card_5()
        case .type6:
            image = R.image.img_card_6()
        }
        if let existingImage = image {
            return UIColor(patternImage: existingImage)
        } else {
            return nil
        }
    }
}

