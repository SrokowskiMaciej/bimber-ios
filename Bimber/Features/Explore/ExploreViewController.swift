//
//  ExploreViewController.swift
//  Bimber
//
//  Created by Maciek on 11.09.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import UIKit

class ExploreViewController: UIViewController {


    private let exploreViewPagerController = ExploreViewPagerController()

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!

    @IBAction func segmentChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0: exploreViewPagerController.set(page: .people)
        case 1:exploreViewPagerController.set(page: .groups)
        default:()
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        segmentedControl.tintColor = BimberColors.colorAccent
        segmentedControl.setTitle(R.string.localizable.view_explore_tab_people(), forSegmentAt: 0)
        segmentedControl.setTitle(R.string.localizable.view_explore_tab_groups(), forSegmentAt: 1)
        addChildViewController(exploreViewPagerController)
        exploreViewPagerController.view.frame = contentView.bounds
        contentView.addSubview(exploreViewPagerController.view)
        exploreViewPagerController.didMove(toParentViewController: self)

        // Do any additional setup after loading the view.
    }
}
