//
// Created by Maciej Srokowski on 4/15/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import UIKit

class ExploreViewPagerController: UIPageViewController {

    public let discoverPeopleViewController: DiscoverPeopleViewController = DiscoverPeopleViewController(viewModel: AppDelegate.container.synchronize().resolve(DiscoverPeopleViewModel.self)!)
    public let discoverGroupsViewController: DiscoverGroupsViewController = DiscoverGroupsViewController()

    init() {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setViewControllers([discoverPeopleViewController], direction: .forward, animated: false)
    }

    override var gestureRecognizers: [UIGestureRecognizer] {
        return []
    }

    public func set(page: ExplorePage) {
        switch page {
        case .people:  setViewControllers([discoverPeopleViewController], direction: .reverse, animated: true)
        case .groups:  setViewControllers([discoverGroupsViewController], direction: .forward, animated: true)
        }

    }

    public enum ExplorePage {
        case people
        case groups
    }
}
