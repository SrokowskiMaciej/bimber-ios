//
//  HomeViewController.swift
//  Bimber
//
//  Created by Maciek on 27.08.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import UIKit

class HomeViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.isTranslucent = false
        let editProfileController = EditProfileViewController(viewModel: AppDelegate.container.synchronize().resolve(EditProfileViewModel.self)!)
        editProfileController.tabBarItem = UITabBarItem(title: "view_home_bottom_navigation_tab_profile".localized,
                image: UIImage(named: "ic_account_circle_white"),
                tag: 0)

        let exploreViewController = ExploreViewController()
        exploreViewController.tabBarItem = UITabBarItem(title: "view_home_bottom_navigation_tab_explore".localized,
                image: UIImage(named: "ic_explore_white"),
                tag: 1)

        let chatListViewController = ChatListViewController()
        chatListViewController.tabBarItem = UITabBarItem(title: "view_home_bottom_navigation_tab_chats".localized,
                image: UIImage(named: "ic_message_white"),
                tag: 2)

        tabBar.tintColor = BimberColors.colorPrimary
        viewControllers = [editProfileController, exploreViewController, chatListViewController]
        selectedIndex = 1
    }
}
