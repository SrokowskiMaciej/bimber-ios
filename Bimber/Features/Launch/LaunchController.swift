//
//  ViewController.swift
//  Bimber
//
//  Created by Maciek on 14.08.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import UIKit
import FirebaseAuth
import os.log


class LaunchController: UIViewController {


    var auth: Auth!


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewWillAppear(_ animated: Bool) {

    }

    override func viewDidAppear(_ animated: Bool) {
        auth.addStateDidChangeListener { (auth, user) in
            if user == nil {
                self.performSegue(withIdentifier: "signInSegue", sender: self)
            } else {
                self.performSegue(withIdentifier: "launcherToLocationPickerSegue", sender: self)
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "launcherToLocationPickerSegue" {
            let viewController = segue.destination as! LocationPickerController
            viewController.continueEnabled = true
        }
    }
}
