//
//  SignInOptionsViewController.swift
//  Bimber
//
//  Created by Maciek on 28.08.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import UIKit
import ActiveLabel

class SignInOptionsViewController: UIViewController {
    
    @IBOutlet weak var labelAgreement: ActiveLabel!
    @IBOutlet weak var buttonSingIn: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!

    override func viewDidLoad() {
        super.viewDidLoad()
                

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
