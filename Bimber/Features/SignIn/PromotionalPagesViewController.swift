//
//  CustomAuthViewController.swift
//  Bimber
//
//  Created by Maciek on 27.08.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import UIKit


protocol PromotionalPageDelegate: class {
    func setPromotionalPage(withIndex index: Int)
}

class PromotionalPagesViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {

    weak var promotionalPageDelegate: PromotionalPageDelegate?

    required init?(coder aDecoder: NSCoder) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        if let firstController = orderedViewControllers.first {
            setViewControllers([firstController],
                                direction: UIPageViewControllerNavigationDirection.forward,
                                animated: true,
                                completion: nil)

        }
    }

    public lazy var orderedViewControllers: [UIViewController] = {
        return [self.newViewController(titleText: "view_sign_in_promotional_text_explore_people".localized, imageResource: "img_promotional_explore_people"),
                self.newViewController(titleText: "view_sign_in_promotional_text_match".localized, imageResource: "img_promotional_match"),
                self.newViewController(titleText: "view_sign_in_promotional_text_chat_person".localized, imageResource: "img_promotional_chat_person"),
                self.newViewController(titleText: "view_sign_in_promotional_text_explore_group".localized, imageResource: "img_promotional_explore_group"),
                self.newViewController(titleText: "view_sign_in_promotional_text_chat_group".localized, imageResource: "img_promotional_chat_group")]
    }()
    
    func newViewController(titleText: String, imageResource: String) -> UIViewController {
        let controller: PromotionalPageViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "promotionalPage") as! PromotionalPageViewController
        controller.titleText = titleText
        controller.imageResource = imageResource
        return controller;
    }
    
    // MARK: Delegate functions
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        // User is on the last view controller and swiped right to loop to
        // the first view controller.
        guard orderedViewControllersCount != nextIndex else {
            return orderedViewControllers.first
            // Uncommment the line below, remove the line above if you don't want the page control to loop.
            // return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        // User is on the first view controller and swiped left to loop to
        // the last view controller.
        guard previousIndex >= 0 else {
            return orderedViewControllers.last
            // Uncommment the line below, remove the line above if you don't want the page control to loop.
            // return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        promotionalPageDelegate?.setPromotionalPage(withIndex: orderedViewControllers.index(of: pageContentViewController)!) 
    }
    
    
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
