//
//  SignInViewController.swift
//  Bimber
//
//  Created by Maciek on 28.08.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import UIKit
import ActiveLabel
import FirebaseAuthUI
import FirebaseGoogleAuthUI
import FirebaseFacebookAuthUI

class SignInViewController: UIViewController, PromotionalPageDelegate, FUIAuthDelegate {
    
    @IBOutlet weak var agreementLabel: ActiveLabel!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var promotionalPages: UIView!
    @IBOutlet weak var pageControl: UIPageControl!

    @IBAction func signInAction(_ sender: Any) {
        self.signIn()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupAgreementLabel()
        setupSignInButton()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let promotionalPagesViewController = segue.destination as? PromotionalPagesViewController {
            promotionalPagesViewController.promotionalPageDelegate = self
            pageControl.numberOfPages = promotionalPagesViewController.orderedViewControllers.count
        }
    }

    private func setupSignInButton() {
        signInButton.setTitle("view_sign_in_button_text".localized, for: .normal)
    }

    private func setupAgreementLabel() {
        let typeFormat = "\\s%@\\b"
        let tosString: String! = "view_sign_in_terms_of_service".localized as String
        let privacyPolicyString = "view_sign_in_privacy_policy".localized as String
        let tosType = ActiveType.custom(pattern: String(format: typeFormat, tosString))
        let privacyPolicyType = ActiveType.custom(pattern: String(format: typeFormat, privacyPolicyString))
        agreementLabel.enabledTypes = [tosType, privacyPolicyType]
        agreementLabel.customColor[tosType] = UIColor.blue
        agreementLabel.customColor[privacyPolicyType] = UIColor.blue
        agreementLabel.text = String.localizedStringWithFormat("view_sign_in_agreement".localized, tosString, privacyPolicyString)
        agreementLabel.handleCustomTap(for: tosType) { (tos) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: "https://bimber.fun/terms_of_service.html")!)
            } else {
                // Fallback on earlier versions
            }
        }
        agreementLabel.handleCustomTap(for: privacyPolicyType) { (privacyPolicy) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: "https://bimber.fun/privacy_policy.html")!)
            } else {
                // Fallback on earlier versions
            }
        }
    }

    private func signIn() {
        let auth = FUIAuth.defaultAuthUI()
        auth?.providers = [FUIGoogleAuth(), FUIFacebookAuth()]
        auth?.delegate = self
        self.present(auth!.authViewController(), animated: false, completion: nil)
    }
    

    func authUI(_ authUI: FUIAuth, didSignInWith user: FirebaseAuth.User?, error: Error?) {
        if user != nil {
            self.performSegue(withIdentifier: "locationPickerSegue", sender: self)
        }
    }
   
    func setPromotionalPage(withIndex index: Int) {
        pageControl.currentPage = index
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
