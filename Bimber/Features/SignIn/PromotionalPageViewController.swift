//
//  PromotionalPageViewController.swift
//  Bimber
//
//  Created by Maciek on 27.08.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import UIKit

class PromotionalPageViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var titleText: String?
    var imageResource: String?
    
   
    public init(titleText: String?, imageResource: String?) {
        self.titleText = titleText
        self.imageResource = imageResource
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = titleText
        imageView.image = UIImage(named: self.imageResource!)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
