//
//  LocationPickerController.swift
//  Bimber
//
//  Created by Maciek on 25.08.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlacePicker
import FirebaseAuth
import RxSwift
import CoreLocation
import LMGeocoder
import CocoaLumberjack

class LocationPickerController: UIViewController, CLLocationManagerDelegate, GMSPlacePickerViewControllerDelegate {


    //MARK: Outlets
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var forwardButton: UIBarButtonItem!
    @IBOutlet weak var titleItem: UINavigationItem!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var selectedLocationLabel: UILabel!

    @IBOutlet weak var currentLocationTitleLabel: UILabel!
    @IBOutlet weak var currentLocationSubtitleLabel: UILabel!
    @IBOutlet weak var currentLocationSwitch: UISwitch!

    @IBOutlet weak var customLocationTitleLabel: UILabel!
    @IBOutlet weak var customLocationSubtitleLabel: UILabel!
    @IBOutlet weak var customLocationSwitch: UISwitch!

    //MARK: Actions
    @IBAction func currentLocationAction(_ sender: UISwitch) {
        if sender.isOn {
            addCurrentLocation()
        } else {
            removeLocation()
        }

    }

    @IBAction func customLocationAction(_ sender: UISwitch) {
        if sender.isOn {
            addCustomLocation()
        } else {
            removeLocation()
        }
    }

    //MARK: Injections
    var locationRepository: UsersLocationNetworkSource!
    var auth: Auth!

    //MARK: Instance members
    let disposeBag = DisposeBag()
    let locationManager = CLLocationManager()

    //MARK: View controller parameters
    var continueEnabled = false

    //MARK: Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()

        selectedLocationLabel.frame.insetBy(dx: 30.0, dy: 30.0)
        titleItem.title = "view_location_picker_title".localized

        currentLocationTitleLabel.text = "view_location_picker_current_location_title".localized
        currentLocationSubtitleLabel.text = "view_location_picker_current_location_subtitle".localized

        customLocationTitleLabel.text = "view_location_picker_custom_location_title".localized
        customLocationSubtitleLabel.text = "view_location_picker_custom_location_subtitle".localized

        backButton.title = "view_location_picker_back_button".localized
        forwardButton.title = "view_location_picker_continue_button".localized

        if (continueEnabled) {
            navBar.topItem?.leftBarButtonItem = nil
            navBar.topItem?.rightBarButtonItem?.action = #selector(self.navigationItemClicked)
        } else {
            navBar.topItem?.rightBarButtonItem = nil
            navBar.topItem?.leftBarButtonItem?.action = #selector(self.navigationItemClicked)
        }
        activityIndicator.hidesWhenStopped = true
        spinner(show: true)
        startListeningToLocation()
    }


    //MARK: Model binding
    private func startListeningToLocation() {
        self.locationRepository.onLocation(ofUser: UserModelId(key: (self.auth.currentUser?.uid)!))
                .subscribe(onNext: { location in
                    self.spinner(show: false)
                    if (location.isEmpty) {
                        self.selectedLocationLabel.text = "view_location_picker_no_location_title".localized
                        let camera = GMSCameraPosition.camera(withLatitude: 0, longitude: 0, zoom: 1)
                        self.mapView.animate(to: camera)
                        self.clearCurrentLocationUi()
                        self.clearCustomLocationUi()
                    } else {
                        self.selectedLocationLabel.text = location[0].locationName
                        let camera = GMSCameraPosition.camera(withLatitude: location[0].latitude, longitude: location[0].longitude, zoom: 10)
                        self.mapView.animate(to: camera)
                        switch location[0].type {
                        case .current:
                            self.setCurrentLocationUi()
                            self.clearCustomLocationUi()

                        case .custom:
                            self.setCustomLocationUi()
                            self.clearCurrentLocationUi()
                        }
                    }
                })
                .disposed(by: self.disposeBag)
    }

    //MARK: Model Actions
    private func addCurrentLocation() {
        self.spinner(show: true)
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestLocation()
        } else {
            //TODO Handle location disabled
        }
    }

    private func addCustomLocation() {
        self.spinner(show: true)
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self

        present(placePicker, animated: true, completion: nil)
    }

    private func removeLocation() {
        locationRepository.removeLocation(ofUser: UserModelId(key: (self.auth.currentUser?.uid)!))
                .subscribe()
                .disposed(by: self.disposeBag)
    }

    //MARK: UI management
    private func setCurrentLocationUi() {
        currentLocationSwitch.setOn(true, animated: true)
    }

    private func setCustomLocationUi() {
        customLocationSwitch.setOn(true, animated: true)
    }

    private func clearCurrentLocationUi() {
        currentLocationSwitch.setOn(false, animated: true)
    }

    private func clearCustomLocationUi() {
        customLocationSwitch.setOn(false, animated: true)
    }

    private func showLocationObtainingError() {
        let alertController = UIAlertController(title: "Error", message: "Couldn't obtain location", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }

    private func spinner(show: Bool) {
        if show {
            self.activityIndicator.startAnimating()
        } else {
            self.activityIndicator.stopAnimating()
        }

    }


    //MARK: Location manager callbacks
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let unwrappedLocation = locations.first {
            Single.just(unwrappedLocation, scheduler: MainScheduler.instance)
                    .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                    .flatMap { location in
                        self.getAddress(coordinates: location.coordinate)
                    }
                    .map { address in
                        LocationModel(key: UserModelId(key: (self.auth.currentUser?.uid)!),
                                latitude: unwrappedLocation.coordinate.latitude,
                                longitude: unwrappedLocation.coordinate.longitude,
                                locationName: address,
                                personUid: UserModelId(key: (self.auth.currentUser?.uid)!),
                                range: 25,
                                type: .current)
                    }
                    .flatMap { location in
                        self.locationRepository.set(location: location).andThen(Single.just(location))
                    }
                    .timeout(5, scheduler: MainScheduler.instance)
                    .observeOn(MainScheduler.instance)
                    .subscribe(onSuccess: { location in

                    },
                            onError: { error in
                                self.clearCurrentLocationUi()
                                self.showLocationObtainingError()
                                self.spinner(show: false)
                                DDLogError(error.localizedDescription)
                                //TODO Show error
                            })
                    .disposed(by: self.disposeBag)
        } else {
            self.clearCurrentLocationUi()
            self.showLocationObtainingError()
            self.spinner(show: false)
            DDLogError("No locations delivered")
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.clearCurrentLocationUi()
        self.showLocationObtainingError()
        self.spinner(show: false)
        DDLogError(error.localizedDescription)
    }

    //MARK: Place picker callbacks
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        viewController.dismiss(animated: true, completion: nil)
        Single.just(place, scheduler: MainScheduler.instance)
                .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .flatMap { location in
                    self.getAddress(coordinates: location.coordinate)
                }
                .map { address in
                    LocationModel(key: UserModelId(key: (self.auth.currentUser?.uid)!),
                            latitude: place.coordinate.latitude,
                            longitude: place.coordinate.longitude,
                            locationName: address,
                            personUid: UserModelId(key: (self.auth.currentUser?.uid)!),
                            range: 25,
                            type: .custom)
                }
                .flatMap { location in
                    self.locationRepository.set(location: location).andThen(Single.just(location))
                }
                .observeOn(MainScheduler.instance)
                .timeout(5, scheduler: MainScheduler.instance)
                .subscribe(onSuccess: { location in

                },
                        onError: { error in
                            self.clearCurrentLocationUi()
                            self.showLocationObtainingError()
                            self.spinner(show: false)
                            DDLogError(error.localizedDescription)
                        })
                .disposed(by: self.disposeBag)
    }

    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
        self.clearCurrentLocationUi()
        self.spinner(show: false)
    }

    //MARK: Reverse geocoding
    private func getAddress(coordinates: CLLocationCoordinate2D) -> Single<String> {
        //TODO Change to apple service before launch
        return reverseGeocode(coordinate: coordinates, service: .googleService)
                .catchError { error in
                    self.reverseGeocode(coordinate: coordinates, service: .googleService)
                }
                .map { address in
                    let city = address.locality ?? "Unknown City"
                    let country = address.country ?? "Unknown Country"
                    return "\(city), \(country)"
                }
                .asObservable()
                .ifEmpty(default: "Unknown City, Unknown Country")
                .catchErrorJustReturn("Unknown City, Unknown Country")
                .asSingle()
    }

    private func reverseGeocode(coordinate: CLLocationCoordinate2D, service: LMGeocoderService) -> Maybe<LMAddress> {
        LMGeocoder.sharedInstance().googleAPIKey = "AIzaSyBHlBJ0ylmsKMImR5I5tVYfXwJs02qHuRU"
        return Maybe.create { maybe in
            LMGeocoder.sharedInstance().reverseGeocodeCoordinate(coordinate,
                    service: service,
                    completionHandler: { (locations, error) in
                        if let error = error {
                            maybe(.error(error))
                        } else if let locations = locations, !locations.isEmpty {
                            maybe(.success(locations[0]))
                        } else {
                            maybe(.completed)
                        }
                    })
            return Disposables.create {
                LMGeocoder.sharedInstance().cancelGeocode()
            }
        }
    }

    @objc public func navigationItemClicked() {
        present(HomeViewController(), animated: true)
    }
}
