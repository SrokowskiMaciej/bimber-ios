//
// Created by Maciej Srokowski on 4/14/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import Kingfisher
import L10n_swift

class EditProfileView: UIView, UICollectionViewDelegateFlowLayout {

    //MARK: Outlets
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var photosTitleLabel: UILabel!
    @IBOutlet weak var photosContentLabel: UILabel!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    @IBOutlet weak var phototsEditButton: UIButton!
    private var photosDataSource: RxCollectionViewSectionedAnimatedDataSource<PhotosSection>!

    @IBOutlet weak var aboutTitleLabel: UILabel!
    @IBOutlet weak var aboutContentLabel: UILabel!
    @IBOutlet weak var aboutEditButton: UIButton!

    @IBOutlet weak var favouriteTreatsTitleLabel: UILabel!
    @IBOutlet weak var favouriteTreatsContentLabel: UILabel!
    @IBOutlet weak var favouriteTreatsCollectionView: UICollectionView!
    @IBOutlet weak var favouriteTreatsAddButton: UIButton!
    private var favouriteTreatsDataSource: RxCollectionViewSectionedAnimatedDataSource<FavouriteTreatsSection>!

    @IBOutlet weak var favouritePlacesTitleLabel: UILabel!
    @IBOutlet weak var favouritePlacesContentLabel: UILabel!
    @IBOutlet weak var favouritePlacesCollectionView: UICollectionView!
    @IBOutlet weak var favouritePlacesAddButton: UIButton!
    private var favouritePlacesDataSource: RxCollectionViewSectionedAnimatedDataSource<FavouritePlacesSection>!

    @IBOutlet weak var genderTitleLabel: UILabel!
    @IBOutlet weak var genderSegmentedControl: UISegmentedControl!

    @IBOutlet weak var ageTitleLabel: UILabel!
    @IBOutlet weak var ageContentLabel: UILabel!
    @IBOutlet weak var ageSlider: UISlider!

    private let disposeBag = DisposeBag()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupFrom(nib: R.nib.editProfileView())
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupFrom(nib: R.nib.editProfileView())
        setup()
    }

    private func setup() {
        setupAboutSection()
        setupPhotosSection()
        setupFavouriteTreatsSection()
        setupFavouritePlacesSection()
        setupGenderSection()
        setupAgeSection()
    }

    private func setupAboutSection() {
        aboutTitleLabel.text = R.string.localizable.view_edit_profile_about_title_text()
        aboutContentLabel.text = R.string.localizable.view_edit_profile_about_hint()
        aboutEditButton.setTitle(R.string.localizable.view_edit_profile_action_edit_about(), for: .normal)
        aboutEditButton.tintColor = BimberColors.colorPrimary
    }

    private func setupPhotosSection() {
        photosTitleLabel.text = R.string.localizable.view_edit_profile_photos_title_text()
        phototsEditButton.setTitle(R.string.localizable.view_edit_profile_action_edit_photos(), for: .normal)
        phototsEditButton.tintColor = BimberColors.colorPrimary
//        photosIcon.image = UIImage(named: "ic_image_white")
//        photosIcon.tintColor = BimberColors.colorPrime


        photosCollectionView.register(PhotoItemCell.self, forCellWithReuseIdentifier: PhotoItemCell.identifier)

        photosDataSource = RxCollectionViewSectionedAnimatedDataSource<PhotosSection>(
                configureCell: { (sources, collectionView, indexPath, userPhoto) -> UICollectionViewCell in
                    let cell: PhotoItemCell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoItemCell.identifier, for: indexPath) as! PhotoItemCell
                    cell.photoView.showDefaultableImage(userPhoto)
                    return cell
                },
                configureSupplementaryView: { (sources, collectionView, identifier, indexPath) -> UICollectionReusableView in
                    return UICollectionReusableView()
                })
    }


    private func setupFavouriteTreatsSection() {
        favouriteTreatsTitleLabel.text = R.string.localizable.view_edit_profile_alcohols_title_text()
        favouriteTreatsContentLabel.text = R.string.localizable.view_edit_profile_alcohols_content_text()
        favouriteTreatsAddButton.setTitle(R.string.localizable.view_edit_profile_action_add_alcohol(), for: .normal)
        favouriteTreatsAddButton.tintColor = BimberColors.colorPrimary

        favouriteTreatsCollectionView.register(FavouriteTreatItemCell.self, forCellWithReuseIdentifier: FavouriteTreatItemCell.identifier)

        favouriteTreatsDataSource = RxCollectionViewSectionedAnimatedDataSource<FavouriteTreatsSection>(
                configureCell: { (sources, collectionView, indexPath, favouriteTreat) -> UICollectionViewCell in
                    let cell: FavouriteTreatItemCell = collectionView.dequeueReusableCell(withReuseIdentifier: FavouriteTreatItemCell.identifier, for: indexPath) as! FavouriteTreatItemCell
                    cell.nameView.text = favouriteTreat.favouriteTreatName
                    let dragGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleFavouriteTreatDragGesture))
                    dragGestureRecognizer.minimumPressDuration = 0
                    cell.dragHandle.addGestureRecognizer(dragGestureRecognizer)
                    return cell
                },
                configureSupplementaryView: { (sources, collectionView, identifier, indexPath) -> UICollectionReusableView in
                    return UICollectionReusableView()
                },
                moveItem: { sources, sourceIndexPath, destinationIndexPath in

                },
                canMoveItemAtIndexPath: { _, _ in
                    return true
                })

        favouriteTreatsCollectionView.delegate = self
    }

    @objc public func handleFavouriteTreatDragGesture(gesture: UILongPressGestureRecognizer) {
        switch (gesture.state) {

        case .began:
            guard let selectedIndexPath = self.favouriteTreatsCollectionView.indexPathForItem(at: gesture.location(in: self.favouriteTreatsCollectionView)) else {
                break
            }
            self.favouriteTreatsCollectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            self.favouriteTreatsCollectionView.updateInteractiveMovementTargetPosition(CGPoint(x: self.favouriteTreatsCollectionView.bounds.width / 2, y: gesture.location(in: self.favouriteTreatsCollectionView).y))
        case .ended:
            self.favouriteTreatsCollectionView.endInteractiveMovement()
        default:
            self.favouriteTreatsCollectionView.cancelInteractiveMovement()
        }
    }

    private func setupFavouritePlacesSection() {
        favouritePlacesTitleLabel.text = R.string.localizable.view_edit_profile_favourite_places_title_text()
        favouritePlacesContentLabel.text = R.string.localizable.view_edit_profile_favourite_places_content_text()
        favouritePlacesAddButton.setTitle(R.string.localizable.view_edit_profile_action_add_place(), for: .normal)
        favouritePlacesAddButton.tintColor = BimberColors.colorPrimary

        favouritePlacesCollectionView.register(FavouritePlaceItemCell.self, forCellWithReuseIdentifier: FavouritePlaceItemCell.identifier)

        favouritePlacesDataSource = RxCollectionViewSectionedAnimatedDataSource<FavouritePlacesSection>(
                configureCell: { (sources, collectionView, indexPath, favouritePlace) -> UICollectionViewCell in
                    let cell: FavouritePlaceItemCell = collectionView.dequeueReusableCell(withReuseIdentifier: FavouritePlaceItemCell.identifier, for: indexPath) as! FavouritePlaceItemCell
                    cell.nameView.text = favouritePlace.favouritePlaceName
                    let dragGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleFavouritePlaceDragGesture))
                    dragGestureRecognizer.minimumPressDuration = 0
                    cell.dragHandle.addGestureRecognizer(dragGestureRecognizer)
                    return cell
                },
                configureSupplementaryView: { (sources, collectionView, identifier, indexPath) -> UICollectionReusableView in
                    return UICollectionReusableView()
                },
                moveItem: { sources, sourceIndexPath, destinationIndexPath in

                },
                canMoveItemAtIndexPath: { _, _ in
                    return true
                })

        favouritePlacesCollectionView.delegate = self
    }

    @objc public func handleFavouritePlaceDragGesture(gesture: UILongPressGestureRecognizer) {
        switch (gesture.state) {

        case .began:
            guard let selectedIndexPath = self.favouritePlacesCollectionView.indexPathForItem(at: gesture.location(in: self.favouritePlacesCollectionView)) else {
                break
            }
            self.favouritePlacesCollectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            self.favouritePlacesCollectionView.updateInteractiveMovementTargetPosition(CGPoint(x: self.favouritePlacesCollectionView.bounds.width / 2, y: gesture.location(in: self.favouritePlacesCollectionView).y))
        case .ended:
            self.favouritePlacesCollectionView.endInteractiveMovement()
        default:
            self.favouritePlacesCollectionView.cancelInteractiveMovement()
        }
    }

    private func setupGenderSection() {
        genderTitleLabel.text = R.string.localizable.view_edit_profile_gender_title_text()
        genderSegmentedControl.setTitle(R.string.localizable.view_edit_profile_gender_content_male_text(), forSegmentAt: 0)
        genderSegmentedControl.setTitle(R.string.localizable.view_edit_profile_gender_content_female_text(), forSegmentAt: 1)
        genderSegmentedControl.tintColor = BimberColors.colorPrimary
    }

    private func setupAgeSection() {
        ageTitleLabel.text = R.string.localizable.view_edit_profile_age_title_text()
        ageContentLabel.text = ""
        ageSlider.tintColor = BimberColors.colorPrimary
    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: 25)
    }

    //MARK: Setters
    public func setAbout(_ about: String) {
        self.aboutContentLabel.text = about
    }


    public func setPhotos(source: Observable<[UserPhoto]>) {
        source.map({ photos in [PhotosSection(items: photos)] })
                .bind(to: photosCollectionView.rx.items(dataSource: photosDataSource))
                .disposed(by: self.disposeBag)

        source.subscribe(onNext: { photos in
                    self.photosContentLabel.text = R.string.localizable.view_edit_profile_photos_content_text(x: photos.count)
                })
                .disposed(by: disposeBag)
    }

    public func setFavouriteTreats(source: Observable<[FavouriteTreat]>) {
        source.map({ favouriteTreats in [FavouriteTreatsSection(items: favouriteTreats)] })
                .bind(to: favouriteTreatsCollectionView.rx.items(dataSource: favouriteTreatsDataSource))
                .disposed(by: self.disposeBag)
    }

    public func setFavouritePlaces(source: Observable<[FavouritePlace]>) {
        source.map({ favouritePlaces in [FavouritePlacesSection(items: favouritePlaces)] })
                .bind(to: favouritePlacesCollectionView.rx.items(dataSource: favouritePlacesDataSource))
                .disposed(by: self.disposeBag)
    }

    public func setGender(_ gender: Gender) {
        switch (gender) {
        case .male:
            self.genderSegmentedControl.selectedSegmentIndex = 0
        case .female:
            self.genderSegmentedControl.selectedSegmentIndex = 1
        }
    }

    public func setAge(_ age: Int) {
        ageContentLabel.text = String(age)
    }

}
