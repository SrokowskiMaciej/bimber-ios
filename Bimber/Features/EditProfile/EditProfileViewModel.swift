//
//  EditProfileViewModel.swift
//  Bimber
//
//  Created by Maciek on 26.10.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import RxSwift
import CocoaLumberjack

struct EditProfileViewModel {

    private let authService: AuthService
    private let userRepository: UsersRepository
    private let userPhotoRepository: UserPhotosRepository
    private let favouritePlacesRepository: FavouritePlacesRepository
    private let favouriteTreatsRepository: FavouriteTreatsRepository

    private let username = Variable<String>("")
    private let userAbout = Variable<String>("")
    private let userGender = Variable<Gender>(.male)
    private let userAge = Variable<Int>(0)
    private let userFriendsCount = Variable<Int>(0)
    private let userPartiesCount = Variable<Int>(0)
    private let userPhotos = Variable<[UserPhoto]>([])
    private let userFavouritePlaces = Variable<[FavouritePlace]>([])
    private let userFavouriteTreats = Variable<[FavouriteTreat]>([])

    private let disposeBag = DisposeBag()

    init(authService: AuthService, userRepository: UsersRepository, userPhotoRepository: UserPhotosRepository, favouritePlacesRepository: FavouritePlacesRepository, favouriteTreatsRepository: FavouriteTreatsRepository) {
        self.authService = authService
        self.userRepository = userRepository
        self.userPhotoRepository = userPhotoRepository
        self.favouritePlacesRepository = favouritePlacesRepository
        self.favouriteTreatsRepository = favouriteTreatsRepository
    }

    public func load() {
        let uId = authService.authenticated()
                .filter({ id in id != nil })
                .map({ id in id! })
                .share(replay: 1)

        uId.flatMapLatest({ id in self.userRepository.on(uId: id) })
                .subscribe(onNext: { users in
                    if let user = users.first {
                        self.username.value = user.displayName
                        self.userAbout.value = user.about
                        self.userFriendsCount.value = user.friends
                        self.userPartiesCount.value = user.parties
                    }
                },
                        onError: { error in
                            DDLogError(error.localizedDescription)
                            //TODO Handle
                        })
                .disposed(by: self.disposeBag)

        uId.flatMapLatest({ id in self.userPhotoRepository.onAllPhotos(ofUser: id) })
                .subscribe(onNext: { userPhotos in
                    self.userPhotos.value = userPhotos
                },
                        onError: { error in
                            DDLogError(error.localizedDescription)
                            //TODO Handle
                        })
                .disposed(by: self.disposeBag)

        uId.flatMapLatest({ id in self.favouritePlacesRepository.onFavouritePlaces(ofUser: id) })
                .subscribe(onNext: { favouritePlaces in self.userFavouritePlaces.value = favouritePlaces },
                        onError: { error in
                            DDLogError(error.localizedDescription)
                            //TODO Handle
                        })
                .disposed(by: self.disposeBag)

        uId.flatMapLatest({ id in self.favouriteTreatsRepository.onFavouriteTreats(ofUser: id) })
                .subscribe(onNext: { favouriteTreats in self.userFavouriteTreats.value = favouriteTreats },
                        onError: { error in
                            DDLogError(error.localizedDescription)
                            //TODO Handle
                        })
                .disposed(by: self.disposeBag)
    }

    public func getName() -> Observable<String> {
        return self.username.asObservable()
    }

    public func getAbout() -> Observable<String> {
        return self.userAbout.asObservable()
    }

    public func getGender() -> Observable<Gender> {
        return self.userGender.asObservable()
    }

    public func getAge() -> Observable<Int> {
        return self.userAge.asObservable()
    }

    public func getFriendsCount() -> Observable<Int> {
        return self.userFriendsCount.asObservable()
    }

    public func getPartiesCount() -> Observable<Int> {
        return self.userPartiesCount.asObservable()
    }

    public func getPhotos() -> Observable<[UserPhoto]> {
        return self.userPhotos.asObservable()
    }

    public func getFavouritePlaces() -> Observable<[FavouritePlace]> {
        return self.userFavouritePlaces.asObservable()
    }

    public func getFavouriteTreats() -> Observable<[FavouriteTreat]> {
        return self.userFavouriteTreats.asObservable()
    }

}
