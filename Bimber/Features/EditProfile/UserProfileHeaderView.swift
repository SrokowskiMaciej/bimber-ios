//
//  ProfileHeaderView.swift
//  Bimber
//
//  Created by Maciek on 14.11.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics
import SnapKit


public class UserProfileHeaderView: UIView {
    public struct Constants {
        static let statusBarHeight: CGFloat = UIApplication.shared.statusBarFrame.height
        static let minHeight: CGFloat = 44 + statusBarHeight
        static let maxHeight: CGFloat = 400.0
        static let profilePictureDiameter: CGFloat = 176
    }

    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = Constants.profilePictureDiameter / 2
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.borderColor = UIColor.white.cgColor
        imageView.layer.borderWidth = 2
        return imageView
    }()

    let nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .white
        label.numberOfLines = 1
        label.font = label.font.withSize(36)
        return label
    }()

    let friendsLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .white
        label.numberOfLines = 1
        return label
    }()

    let friendsIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = R.image.ic_person_white()
        return imageView
    }()

    let partiesLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .white
        label.numberOfLines = 1
        return label
    }()

    let partiesIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = R.image.ic_group_white()
        return imageView
    }()

    init() {
        super.init(frame: .zero)
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
        clipsToBounds = true
        configureView()
    }

    // 2
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: View

    func configureView() {
        backgroundColor = .darkGray
        addSubview(profileImageView)
        addSubview(nameLabel)
        addSubview(friendsLabel)
        addSubview(friendsIcon)
        addSubview(partiesLabel)
        addSubview(partiesIcon)
    }

    // 4
    override public func layoutSubviews() {
        super.layoutSubviews()
        profileImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(Constants.profilePictureDiameter)
            make.bottom.equalTo(self).inset(Constants.minHeight)
            make.centerX.equalTo(self)

        }
        nameLabel.snp.makeConstraints { make in
            make.bottom.equalTo(self).inset(5)
            make.left.right.equalTo(self).inset(25)
        }

        friendsLabel.snp.makeConstraints({ make in
            make.bottom.equalTo(self).inset(Constants.minHeight)
            make.right.equalTo(self.profileImageView.snp.left).inset(20)
        })

        friendsIcon.snp.makeConstraints({ make in
            make.bottom.equalTo(friendsLabel.snp.top)
            make.centerX.equalTo(friendsLabel)
        })

        partiesLabel.snp.makeConstraints({ make in
            make.bottom.equalTo(self).inset(Constants.minHeight)
            make.left.equalTo(self.profileImageView.snp.right).inset(20)
        })

        partiesIcon.snp.makeConstraints({ make in
            make.bottom.equalTo(partiesLabel.snp.top)
            make.centerX.equalTo(partiesLabel)
        })
    }
}

