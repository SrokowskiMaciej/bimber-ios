//
//  EditProfileContentCollections.swift
//  Bimber
//
//  Created by Maciek on 09.11.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources
import Kingfisher

extension UserPhoto: IdentifiableType {
    public typealias Identity = UserPhotoId
    public var identity: UserPhotoId {
        return self.photoId
    }
}

extension FavouriteTreat: IdentifiableType {
    public typealias Identity = FavouriteTreatId
    public var identity: Identity {
        return self.favouriteTreatId
    }
}

extension FavouritePlace: IdentifiableType {
    public typealias Identity = FavouritePlaceId
    public var identity: Identity {
        return self.favouritePlaceId
    }
}

struct PhotosSection {
    let items: [UserPhoto]
}

struct FavouriteTreatsSection {
    let items: [FavouriteTreat]
}

struct FavouritePlacesSection {
    let items: [FavouritePlace]
}

extension PhotosSection: AnimatableSectionModelType {
    typealias Identity = String
    public var identity: String {
        return "photosSection"
    }
    typealias Item = UserPhoto

    init(original: PhotosSection, items: [UserPhoto]) {
        self.items = items
    }
}

extension FavouriteTreatsSection: AnimatableSectionModelType {
    typealias Identity = String
    public var identity: String {
        return "favouriteTreatsSection"
    }
    typealias Item = FavouriteTreat

    init(original: FavouriteTreatsSection, items: [FavouriteTreat]) {
        self.items = items
    }
}

extension FavouritePlacesSection: AnimatableSectionModelType {
    typealias Identity = String
    public var identity: String {
        return "favouritePlacesSection"
    }
    typealias Item = FavouritePlace

    init(original: FavouritePlacesSection, items: [FavouritePlace]) {
        self.items = items
    }
}


public class PhotoItemCell: UICollectionViewCell {

    static let identifier = "PhotoItemCellIdentifier"

    public lazy var photoView: UIImageView = {
        let photoView = UIImageView(frame: CGRect(x: 0, y: 0, width: 64, height: 64))
        photoView.layer.masksToBounds = true
        photoView.layer.cornerRadius = 2
        return photoView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(photoView)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

public class FavouriteTreatItemCell: UICollectionViewCell {

    static let identifier = "FavouriteTreatCellIdentifier"

    public lazy var nameView: UILabel = {
        let nameView = UILabel()
        nameView.lineBreakMode = .byTruncatingTail
        nameView.font = nameView.font.withSize(13)
        return nameView
    }()

    public lazy var deleteButton: UIButton = {
        let mapButton = UIButton(type: .detailDisclosure)
        mapButton.setImage(UIImage(named: "ic_delete_white"), for: .normal)
        mapButton.tintColor = BimberColors.colorPrimary
        return mapButton
    }()

    public lazy var dragHandle: UIButton = {
        let mapButton = UIButton(type: .detailDisclosure)
        mapButton.setImage(UIImage(named: "ic_drag_handle_white"), for: .normal)
        mapButton.tintColor = BimberColors.colorPrimary
        return mapButton
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(nameView)
        contentView.addSubview(deleteButton)
        contentView.addSubview(dragHandle)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func layoutSubviews() {
        nameView.snp.makeConstraints { make in
            make.left.top.bottom.equalTo(self)
            make.right.equalTo(self.deleteButton.snp.left)
        }

        dragHandle.snp.makeConstraints { make in
            make.width.height.equalTo(32)
            make.width.height.equalTo(32)
            make.right.top.bottom.equalTo(self)
        }

        deleteButton.snp.makeConstraints { make in
            make.width.height.equalTo(32)
            make.right.equalTo(self.dragHandle.snp.left)
            make.top.bottom.equalTo(self)
        }
    }
}

public class FavouritePlaceItemCell: UICollectionViewCell {

    static let identifier = "FavouritePlaceCellIdentifier"

    public lazy var nameView: UILabel = {
        let nameView = UILabel()
        nameView.lineBreakMode = .byTruncatingTail
        nameView.font = nameView.font.withSize(13)
        return nameView
    }()

    public lazy var mapButton: UIButton = {
        let mapButton = UIButton(type: .detailDisclosure)
        mapButton.setImage(UIImage(named: "ic_map_white"), for: .normal)
        mapButton.tintColor = BimberColors.colorPrimary
        return mapButton
    }()

    public lazy var deleteButton: UIButton = {
        let mapButton = UIButton(type: .detailDisclosure)
        mapButton.setImage(UIImage(named: "ic_delete_white"), for: .normal)
        mapButton.tintColor = BimberColors.colorPrimary
        return mapButton
    }()

    public lazy var dragHandle: UIButton = {
        let mapButton = UIButton(type: .detailDisclosure)
        mapButton.setImage(UIImage(named: "ic_drag_handle_white"), for: .normal)
        mapButton.tintColor = BimberColors.colorPrimary
        return mapButton
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(nameView)
        contentView.addSubview(mapButton)
        contentView.addSubview(deleteButton)
        contentView.addSubview(dragHandle)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func layoutSubviews() {
        nameView.snp.makeConstraints { make in
            make.left.top.bottom.equalTo(self)
            make.right.equalTo(self.mapButton.snp.left)
        }

        dragHandle.snp.makeConstraints { make in
            make.width.height.equalTo(32)
            make.right.top.bottom.equalTo(self)
        }

        deleteButton.snp.makeConstraints { make in
            make.width.height.equalTo(32)
            make.right.equalTo(self.dragHandle.snp.left)
            make.top.bottom.equalTo(self)
        }

        mapButton.snp.makeConstraints { make in
            make.width.height.equalTo(32)
            make.right.equalTo(self.deleteButton.snp.left)
            make.top.bottom.equalTo(self)
        }
    }
}
