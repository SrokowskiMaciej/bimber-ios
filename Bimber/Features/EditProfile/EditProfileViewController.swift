//
//  EditProfileViewController.swift
//  Bimber
//
//  Created by Maciek on 11.09.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import UIKit
import FirebaseAuth
import RxSwift
import RxCocoa
import RxDataSources
import Kingfisher
import L10n_swift
import MaterialComponents
import CocoaLumberjack

class EditProfileViewController: UIViewController, UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet weak var editProfileView: EditProfileView!
    
    //MARK: Injections
    var viewModel: EditProfileViewModel!

    //MARK: Instance members
    let appBar = MDCAppBar()
    let headerView = UserProfileHeaderView()
    let disposeBag = DisposeBag()

    init(viewModel: EditProfileViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(nibName: nil, bundle: nil)
    }
    
    //MARK: Lifecycles
    private func view() -> EditProfileView {
        return self.editProfileView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupAppBar()
        setupEditProfileView()
        viewModel.load()
    }

    private func setupAppBar() {

        headerView.backgroundColor = BimberColors.colorPrimary
        // 1
        self.addChildViewController(appBar.headerViewController)

        // 2
        appBar.navigationBar.backgroundColor = .clear
        appBar.navigationBar.title = ""

        // 3
        let appBarHeaderView = appBar.headerViewController.headerView
        appBarHeaderView.backgroundColor = BimberColors.colorPrimary
        appBarHeaderView.maximumHeight = appBarHeaderView.bounds.width * 0.8
        appBarHeaderView.minimumHeight = UserProfileHeaderView.Constants.minHeight

        // 4
        headerView.frame = appBarHeaderView.bounds
        appBarHeaderView.insertSubview(headerView, at: 0)

        // 5
        appBarHeaderView.trackingScrollView = view().scrollView
        view().scrollView.delegate = appBar.headerViewController

        // 6
        appBar.addSubviewsToParent()

        var frame = view().scrollView.frame
        frame.origin.y = 0;
        //pass the cordinate which you want
        frame.origin.x = frame.origin.x;
        //pass the cordinate which you want
        view().scrollView.frame = frame

        viewModel.getName()
                .subscribe(onNext: { name in
                    self.headerView.nameLabel.text = name
                })
                .disposed(by: self.disposeBag)

        viewModel.getPhotos()
                .map({ photos in photos.first })
                .subscribe(onNext: { photo in
                    if let existingPhoto = photo {
                        self.headerView.profileImageView.showDefaultableImage(existingPhoto)
                    }
                })
                .disposed(by: self.disposeBag)

        viewModel.getFriendsCount()
                .subscribe(onNext: { friendsCount in
                    self.headerView.friendsLabel.text =   R.string.localizable.profile_friends_count(x: friendsCount)
                })
                .disposed(by: self.disposeBag)

        viewModel.getPartiesCount()
                .subscribe(onNext: { partiesCount in
                    self.headerView.partiesLabel.text =  R.string.localizable.profile_parties_count(x: partiesCount)
                })
                .disposed(by: self.disposeBag)
    }

    private func setupEditProfileView() {
        viewModel.getAbout()
                .subscribe(onNext: { about in self.view().setAbout(about) })
                .disposed(by: disposeBag)

        viewModel.getGender()
                .subscribe(onNext: { gender in self.view().setGender(gender) })
                .disposed(by: self.disposeBag)

        viewModel.getAge()
                .subscribe(onNext: { age in self.view().setAge(age) })
                .disposed(by: self.disposeBag)

        view().setPhotos(source: viewModel.getPhotos())
        view().setFavouritePlaces(source: viewModel.getFavouritePlaces())
        view().setFavouriteTreats(source: viewModel.getFavouriteTreats())
    }

}

