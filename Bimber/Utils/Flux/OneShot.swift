//
// Created by Maciej Srokowski on 24/08/2018.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation

struct OneShot<T: Equatable>: Equatable {
    private var value: T?

    init(_ value: T) {
        self.value = value
    }

    init() {
        self.value = nil
    }

    mutating func get(_ execute: (T) -> Void) {
        if let local = self.value {
            self.value = nil
            execute(local)
        }
    }
}
