//
//  SwipeView?Layou?t.swift
//  Bimber
//
//  Created by Maciej Srokowski on 15/07/2018.
//  Copyright © 2018 bimber. All rights reserved.
//

import Foundation
import UIKit
import CocoaLumberjack

class SwipeViewLayout: UICollectionViewLayout, UIGestureRecognizerDelegate {

//    var delegate: CardStackDelegate?

    /// The maximum number of cards displayed on the top stack. This value includes the currently visible card.
    @IBInspectable open var stackMaximumSize: Int = 3

    @IBInspectable open var minScale: CGFloat = 0.85
    @IBInspectable open var maxScale: CGFloat = 1.0
    // The inset or outset margins for the rectangle around the card.
    @IBInspectable open var cardInsets: UIEdgeInsets = .zero

    private var panGestureRecognizer: UIPanGestureRecognizer?

    private var animations: [DraggedItem] = []
    fileprivate var draggedItem: DraggedItem? = nil

    var delegate: SwipeViewLayoutDelegate? = nil
    var allowedDirections: Set<SwipeDirection> = [] {
        didSet {
            self.allowedLeft = allowedDirections.contains(.left)
            self.allowedTop = allowedDirections.contains(.top)
            self.allowedRight = allowedDirections.contains(.right)
            self.allowedBottom = allowedDirections.contains(.bottom)
        }
    }
    private var allowedLeft = false
    private var allowedTop = false
    private var allowedRight = false
    private var allowedBottom = false

    // MARK: - Getting the Collection View Information

    override open var collectionViewContentSize: CGSize {
        return collectionView!.bounds.size
    }

    fileprivate func getCollectionViewCenter() -> CGPoint {
        return CGPoint(x: collectionViewContentSize.width / 2, y: collectionViewContentSize.height / 2)
    }

    fileprivate func cardFrame() -> CGRect {
        let center = getCollectionViewCenter()
        let width: CGFloat = self.collectionViewContentSize.width
        let height: CGFloat = self.collectionViewContentSize.height
        return CGRect(x: center.x - width / 2, y: center.y - height / 2, width: width, height: height)
    }

    fileprivate func updateCardFrame(frame: inout CGRect, scale: CGFloat) -> CGRect {
        let width: CGFloat = self.collectionViewContentSize.width * scale
        let height: CGFloat = self.collectionViewContentSize.height * scale
        frame.size.width = width
        frame.size.height = height
        return frame
    }

    override func prepare() {
        super.prepare()
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(SwipeViewLayout.handlePan(_:)))
        collectionView?.addGestureRecognizer(panGestureRecognizer!)
        panGestureRecognizer!.delegate = self
    }

    func reverseItem(at index: IndexPath) {

        // TODO
    }

    func swipe(inDirection: SwipeDirection) {
        // TODO
    }

    // MARK: - Providing Layout Attributes
    open override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return (0..<min(collectionView!.numberOfItems(inSection: 0), stackMaximumSize))
                .map({ index in IndexPath(item: index, section: 0) })
                .map({ indexPath in self.layoutAttributesForItem(at: indexPath) })
                .compactMap({ $0 })
    }

    open override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let result = UICollectionViewLayoutAttributes(forCellWith: indexPath)

        if indexPath.item == 0 {
            result.frame = cardFrame()
        } else if let previousDraggedItem = self.draggedItem, previousDraggedItem.indexPath.item == indexPath.item + 1 {
            let scaleForNextItem = self.getScale(dx: previousDraggedItem.dx(), dy: previousDraggedItem.dy())
            result.frame = cardFrame()
            result.transform = CGAffineTransform(scaleX: scaleForNextItem, y: scaleForNextItem)
        } else if let previousAnimatedItem = self.animations.first(where: { $0.indexPath.item == indexPath.item + 1 }) {
            let scaleForNextItem = self.getScale(dx: previousAnimatedItem.dx(), dy: previousAnimatedItem.dy())
            result.frame = cardFrame()
            result.transform = CGAffineTransform(scaleX: scaleForNextItem, y: scaleForNextItem)
        } else {
            result.frame = cardFrame()
            result.transform = CGAffineTransform(scaleX: self.minScale, y: self.minScale)
        }
        result.zIndex = -indexPath.item
        return result
    }

    override open func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let result = UICollectionViewLayoutAttributes(forCellWith: itemIndexPath)

        if itemIndexPath.item == 0 {
            result.frame = cardFrame()
        } else if let previousDraggedItem = self.draggedItem, previousDraggedItem.indexPath.item == itemIndexPath.item + 1 {
            let scaleForNextItem = getScale(dx: previousDraggedItem.dx(), dy: previousDraggedItem.dy())
            result.frame = cardFrame()
            result.transform = CGAffineTransform(scaleX: scaleForNextItem, y: scaleForNextItem)
        } else if let previousAnimatedItem = self.animations.first(where: { $0.indexPath.item == itemIndexPath.item + 1 }) {
            let scaleForNextItem = getScale(dx: previousAnimatedItem.dx(), dy: previousAnimatedItem.dy())
            result.frame = cardFrame()
            result.transform = CGAffineTransform(scaleX: scaleForNextItem, y: scaleForNextItem)
        } else {
            result.frame = cardFrame()
            result.transform = CGAffineTransform(scaleX: self.minScale, y: self.minScale)
        }
        result.zIndex = -itemIndexPath.item
        return result
    }


    override func prepare(forCollectionViewUpdates updateItems: [UICollectionViewUpdateItem]) {
        super.prepare(forCollectionViewUpdates: updateItems)
        updateItems.forEach { item in
            switch item.updateAction {
            case .insert:
                ()
            case .delete:
                if let deletedIndex = item.indexPathBeforeUpdate {
                    if self.draggedItem?.indexPath == deletedIndex {
                        self.draggedItem = nil
                    }
                    removeAnimation(deletedIndex: deletedIndex)
                }
            case .reload:
                self.draggedItem = nil
                self.animations.removeAll()
            case .move:
                if let originIndex = item.indexPathBeforeUpdate,
                   let targetIndex = item.indexPathAfterUpdate {
                    if self.draggedItem?.indexPath == originIndex {
                        self.draggedItem?.indexPath = targetIndex
                    }
                    for i in self.animations.indices {
                        if (self.animations[i].indexPath == originIndex) {
                            self.animations[i].indexPath = targetIndex
                            break;
                        }
                    }
                    if let attributes = collectionView?.layoutAttributesForItem(at: originIndex) {
                        attributes.zIndex = -targetIndex.item
                    }
                }
            case .none:
                ()
            }
        }
    }

    fileprivate func signalProgress(indexPath: IndexPath, dx: CGFloat, dy: CGFloat) {
        let direction = getPossibleDirection(dx: dx, dy: dy)
        let progress = getProgress(dx: dx, dy: dy)
        switch direction {
        case .left:
            delegate?.on(swipeProgress: progress, in: .left, at: indexPath)
            delegate?.on(swipeProgress: 0, in: .top, at: indexPath)
            delegate?.on(swipeProgress: 0, in: .right, at: indexPath)
            delegate?.on(swipeProgress: 0, in: .bottom, at: indexPath)
        case .top:
            delegate?.on(swipeProgress: 0, in: .top, at: indexPath)
            delegate?.on(swipeProgress: progress, in: .top, at: indexPath)
            delegate?.on(swipeProgress: 0, in: .right, at: indexPath)
            delegate?.on(swipeProgress: 0, in: .bottom, at: indexPath)
        case .right:
            delegate?.on(swipeProgress: 0, in: .left, at: indexPath)
            delegate?.on(swipeProgress: 0, in: .top, at: indexPath)
            delegate?.on(swipeProgress: progress, in: .right, at: indexPath)
            delegate?.on(swipeProgress: 0, in: .bottom, at: indexPath)
        case .bottom:
            delegate?.on(swipeProgress: 0, in: .left, at: indexPath)
            delegate?.on(swipeProgress: 0, in: .top, at: indexPath)
            delegate?.on(swipeProgress: 0, in: .right, at: indexPath)
            delegate?.on(swipeProgress: progress, in: .bottom, at: indexPath)
        }

    }

// MARK: Progress

    fileprivate func getProgress(dx: CGFloat, dy: CGFloat) -> CGFloat {
        let collectionHalfWidth = collectionViewContentSize.width / 2
        let hypotenuse = (dx * dx + dy * dy).squareRoot()
        let ratio: CGFloat = hypotenuse / collectionHalfWidth
        return min(ratio, 1.0)
    }

// MARK: Scale

    fileprivate func getScale(dx: CGFloat, dy: CGFloat) -> CGFloat {
        let progress = getProgress(dx: dx, dy: dy)
        return self.minScale + ((maxScale - minScale) * progress)
    }

// MARK Rotation

    fileprivate func getRotation(for item: DraggedItem) -> CGFloat {
        return getRotation(forXPosition: item.currentCenter().x)
    }

    fileprivate func getRotation(forXPosition x: CGFloat) -> CGFloat {
        let opposite = x - collectionViewContentSize.width / 2
        let adjacent = collectionViewContentSize.height * 2.0
        return atan(opposite / adjacent)
    }

    // MARK: Directions

    fileprivate func isDirectionAllowed(dx: CGFloat, dy: CGFloat) -> Bool {
        if (dx > dy) {
            if (dx > -dy) {
                return self.allowedRight;
            } else {
                return self.allowedTop;
            }
        } else {
            if (dx < -dy) {
                return self.allowedLeft;
            } else {
                return self.allowedBottom;
            }
        }
    }

    fileprivate func getPossibleDirection(dx: CGFloat, dy: CGFloat) -> SwipeDirection {
        switch (self.allowedLeft, self.allowedTop, self.allowedRight, self.allowedBottom) {
        case (true, false, true, false):
            return dx > 0 ? .right : .left
        case (true, false, true, true):
            if (dx > dy) {
                if (dx > 0) {
                    return .right;
                } else {
                    return .left;
                }
            } else {
                if (dx < -dy) {
                    return .left;
                } else {
                    return .bottom;
                }
            }
        default:
            // TODO
            fatalError("TODO: Handle other combinations")
        }
    }

// MARK: - Handling the Swipe and Pan Gesture

    @objc internal func handlePan(_ sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .possible, .failed:
            ()
        case .began:
            let initialPanPoint = sender.location(in: collectionView)
            start(draggedItem: &self.draggedItem, with: initialPanPoint)
        case .changed:
            let newCenter = sender.location(in: collectionView!)
            update(draggedItem: &self.draggedItem, withTouch: newCenter, andVelocity: sender.velocity(in: collectionView!))
        case .cancelled, .ended:
            finish(draggedItem: &self.draggedItem)
        }
    }

//Define what card should we drag
    fileprivate func start(draggedItem: inout DraggedItem?, with touchCoordinate: CGPoint) {
        let topCell = collectionView?.visibleCells
                .filter { (cell: UICollectionViewCell) -> Bool in
                    cell.frame.contains(touchCoordinate)
                }
                .sorted { (cell: UICollectionViewCell, cell2: UICollectionViewCell) -> Bool in
                    cell.layer.zPosition > cell2.layer.zPosition
                }
                .first

        if let existingTopCell = topCell,
           let indexPath = collectionView?.indexPath(for: existingTopCell),
           let initialCenterCoordinate = collectionView?.cellForItem(at: indexPath)?.center {
            draggedItem = DraggedItem(initialTouch: touchCoordinate, initialCenter: initialCenterCoordinate, indexPath: indexPath, currentTouch: touchCoordinate, velocity: CGPoint.zero)
            if let item = draggedItem {
                signalProgress(indexPath: item.indexPath, dx: item.dx(), dy: item.dy())
            }
        }
    }

//Change position of dragged card
    fileprivate func update(draggedItem: inout DraggedItem?, withTouch touchCoordinate: CGPoint, andVelocity velocity: CGPoint) {
        draggedItem?.currentTouch = touchCoordinate
        draggedItem?.velocity = velocity
        if let draggedItemUnpacked = draggedItem,
           let cell = collectionView?.cellForItem(at: draggedItemUnpacked.indexPath) {
            if let draggedItemUnpacked = draggedItem,
               let currentCenter = draggedItem?.currentCenter() {
                cell.center = currentCenter
                let rotation = CGAffineTransform(rotationAngle: getRotation(for: draggedItemUnpacked))
                cell.transform = rotation
                if let cell = self.collectionView?.cellForItem(at: IndexPath(item: draggedItemUnpacked.indexPath.item + 1, section: 0)) {
                    let scaleForNextItem = self.getScale(dx: draggedItemUnpacked.dx(), dy: draggedItemUnpacked.dy())
                    cell.transform = CGAffineTransform(scaleX: scaleForNextItem, y: scaleForNextItem)
                }
                signalProgress(indexPath: draggedItemUnpacked.indexPath, dx: draggedItemUnpacked.dx(), dy: draggedItemUnpacked.dy())
            }
        }
    }

    fileprivate func finish(draggedItem: inout DraggedItem?) {
        if let draggedItemUnpacked = draggedItem,
           let cell = collectionView?.cellForItem(at: draggedItemUnpacked.indexPath) {
            let terminalVelocity = (pow(draggedItemUnpacked.velocity.x, 2) + pow(draggedItemUnpacked.velocity.y, 2)).squareRoot()
            let isDirectionAllowed = self.isDirectionAllowed(dx: draggedItemUnpacked.velocity.x, dy: draggedItemUnpacked.velocity.y)
            if (terminalVelocity > 400.0 && isDirectionAllowed) {

                let collectionViewCenter = getCollectionViewCenter()
                let currentCenter: CGPoint = draggedItemUnpacked.currentCenter()

                let initialVelocity = (pow(draggedItemUnpacked.velocity.x, 2.0) + pow(draggedItemUnpacked.velocity.y, 2.0)).squareRoot()
                let xVelocityShare = draggedItemUnpacked.velocity.x / initialVelocity
                let yVelocityShare = draggedItemUnpacked.velocity.y / initialVelocity
                let velocityLimit: CGFloat = 600.0
                let travelDistance = (pow(collectionViewContentSize.width, 2.0) + pow(collectionViewContentSize.height, 2.0)).squareRoot()
                let limitedVelocity = initialVelocity > velocityLimit ? velocityLimit : initialVelocity
                let travelTime = travelDistance / limitedVelocity

                let targetX = currentCenter.x + (travelDistance * xVelocityShare)
                let targetY = currentCenter.y + (travelDistance * yVelocityShare)
                let targetRotation = CGAffineTransform(rotationAngle: getRotation(forXPosition: targetX))
                let targetCenter = CGPoint(x: targetX, y: targetY)
                let direction = self.getPossibleDirection(dx: draggedItemUnpacked.velocity.x, dy: draggedItemUnpacked.velocity.y)

                UIView.animate(withDuration: TimeInterval(travelTime),
                        delay: 0.0,
                        usingSpringWithDamping: 1.0,
                        initialSpringVelocity: 0.0,
                        options: [],
                        animations: { () -> Void in
                            cell.center = targetCenter
                            cell.transform = targetRotation
                            if let cell = self.collectionView?.cellForItem(at: IndexPath(item: draggedItemUnpacked.indexPath.item + 1, section: 0)) {
                                cell.transform = CGAffineTransform(scaleX: self.maxScale, y: self.maxScale)
                            }
                            self.signalProgress(indexPath: draggedItemUnpacked.indexPath, dx: targetX - collectionViewCenter.x, dy: targetY - collectionViewCenter.y)
                        },
                        completion: { finished in
                            self.removeAnimation(deletedIndex: draggedItemUnpacked.indexPath)
                            self.delegate?.onItemSwiped(inDirection: direction, at: draggedItemUnpacked.indexPath)
                        })
            } else {
                let collectionViewCenter = getCollectionViewCenter()
                let startRotation = CGAffineTransform(rotationAngle: CGFloat(0.0))
                let returnTime = 0.4
                self.animations.append(draggedItemUnpacked)
                draggedItem = nil
                UIView.animate(withDuration: returnTime,
                        delay: 0.0,
                        usingSpringWithDamping: 0.7,
                        initialSpringVelocity: 0.0,
                        options: [],
                        animations: { () -> Void in
                            cell.center = collectionViewCenter
                            cell.transform = startRotation
                            if let nextCell = self.collectionView?.cellForItem(at: IndexPath(item: draggedItemUnpacked.indexPath.item + 1, section: 0)) {
                                nextCell.transform = CGAffineTransform(scaleX: self.minScale, y: self.minScale)
                            }
                            self.signalProgress(indexPath: draggedItemUnpacked.indexPath, dx: 0, dy: 0)
                        },
                        completion: { finished in
                            self.removeAnimation(deletedIndex: draggedItemUnpacked.indexPath)
                        })
            }
        }
    }

// MARK: - Animation management

    private func removeAnimation(deletedIndex: IndexPath) {
        for i in self.animations.indices {
            if (self.animations[i].indexPath == deletedIndex) {
                self.animations.remove(at: i)
                break;
            }
        }
    }

    //MARK: - UIGestureRecognizerDelegate

    open func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    fileprivate struct DraggedItem {
        let initialTouch: CGPoint
        let initialCenter: CGPoint
        // Index path may be variable as items can be inserted or deleted
        var indexPath: IndexPath
        var currentTouch: CGPoint
        var velocity: CGPoint

        func currentCenter() -> CGPoint {
            return CGPoint(x: initialCenter.x + currentTouch.x - initialTouch.x, y: initialCenter.y + currentTouch.y - initialTouch.y)
        }

        func dx() -> CGFloat {
            return currentTouch.x - initialTouch.x
        }

        func dy() -> CGFloat {
            return currentTouch.y - initialTouch.y
        }
    }

}

protocol SwipeViewLayoutDelegate {
    func on(swipeProgress: CGFloat, in direction: SwipeDirection, at: IndexPath)

    func onItemSwiped(inDirection direction: SwipeDirection, at indexPath: IndexPath)
}

enum SwipeDirection {
    case left
    case top
    case right
    case bottom
}

