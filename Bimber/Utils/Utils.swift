//
//  Utils.swift
//  Bimber
//
//  Created by Maciek on 04.12.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation

extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}

enum CustomError: Error {
    case runtimeError(String)
    case nonImplementedError(String)
    case missingFieldError(String)
}
