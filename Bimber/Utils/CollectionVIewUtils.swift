//
//  CollectionVIewUtils.swift
//  Bimber
//
//  Created by Maciek on 25.11.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics

public class SingleColumnCollectionViewDelegateFlowLayout: NSObject, UICollectionViewDelegateFlowLayout {

    @objc public func collectionView(_ collectionView: UICollectionView,
                                     layout collectionViewLayout: UICollectionViewLayout,
                                     sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionViewLayout.collectionViewContentSize.height)
    }

}
