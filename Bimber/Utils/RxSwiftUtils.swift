//
// Created by Maciek on 13/02/2018.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import RxSwift
import RxSwiftExt

public extension Observable {
    public func listen() -> Observable<Entity> {
        return self.map { _ in
                    Entity.instance
                }
                .distinctUntilChanged()
                .startWith(Entity.instance)
    }


}

public enum Entity {
    case instance
}

//TODO Fix after solution is proposed for https://github.com/ReactiveX/RxSwift/issues/1329
public extension ObservableType where E: Sequence, E.Iterator.Element: Equatable {
    func distinctUntilChanged() -> Observable<E> {
        return distinctUntilChanged { (lhs, rhs) -> Bool in
            return Array(lhs) == Array(rhs)
        }
    }
}

extension ObservableType {
    func ofType<T>(_ caster: (E) -> T) -> Observable<T> {
        return self.filter {
            $0 is T
        }.map {
            $0 as! T
        }
    }
}

public extension ObservableType where E: Sequence {

    func aggregate<Key, Result>(keySelector: @escaping (E.Iterator.Element) -> Key,
                                mapping: @escaping (Key) -> Observable<Result>) -> Observable<[Result]> where Key: Hashable {


        let multicastedSelf: Observable<E> = self.share(replay: 1)

        let availableMappedValues: Observable<[Key: Result]> = multicastedSelf.flatMap({ inputs in Observable.from(inputs) })
                .map(keySelector)
                .distinct()
                .flatMap({ key in mapping(key).map({ mappedValue in (key, mappedValue) }) })
                .scan(Dictionary<Key, Result>(), accumulator: { (collectedValues: [Key: Result], newValue: (Key, Result)) in
                    var updatedCollectedValues = collectedValues
                    updatedCollectedValues[newValue.0] = newValue.1
                    return updatedCollectedValues
                })
                .startWith([:])

        return Observable.combineLatest(multicastedSelf, availableMappedValues,
                resultSelector: { (inputs: E, availableOutputs: [Key: Result]) -> [Result] in
                    var results = Array<Result>()
                    for input in inputs {
                        if let result = availableOutputs[keySelector(input)] {
                            results.append(result)
                        }
                    }
                    return results
                })
    }

    func aggregate<Key, Result, Output>(keySelector: @escaping (E.Iterator.Element) -> Key,
                                        mapping: @escaping (Key) -> Observable<Result>,
                                        postMapping: @escaping (E.Iterator.Element, Result) -> Output) -> Observable<[Output]> where Key: Hashable {


        let multicastedSelf: Observable<E> = self.share(replay: 1)

        let availableMappedValues: Observable<[Key: Result]> = multicastedSelf.flatMap({ inputs in Observable.from(inputs) })
                .map(keySelector)
                .distinct()
                .flatMap({ key in mapping(key).map({ mappedValue in (key, mappedValue) }) })
                .scan(Dictionary<Key, Result>(), accumulator: { (collectedValues: [Key: Result], newValue: (Key, Result)) in
                    var updatedCollectedValues = collectedValues
                    updatedCollectedValues[newValue.0] = newValue.1
                    return updatedCollectedValues
                })
                .startWith([:])

        return Observable.combineLatest(multicastedSelf, availableMappedValues,
                resultSelector: { (inputs: E, availableOutputs: [Key: Result]) -> [Output] in
                    var results = Array<Output>()
                    for input in inputs {
                        if let result = availableOutputs[keySelector(input)] {
                            results.append(postMapping(input, result))
                        }
                    }
                    return results
                })
    }
}

extension ObservableType {
    func aggregateFrom<Value, Key, Result, Output>(unpack: @escaping (E) -> [Value],
                                                   keySelector: @escaping (Value) -> Key,
                                                   mapping: @escaping (Key) -> Observable<Result>,
                                                   repack: @escaping (E, [Result]) -> Output) -> Observable<Output> where Key: Hashable {


        let multicastedSelf: Observable<E> = self.share(replay: 1)

        let availableMappedValues: Observable<[Key: Result]> = multicastedSelf.flatMap({ input in Observable.from(unpack(input)) })
                .map(keySelector)
                .distinct()
                .flatMap({ key in mapping(key).map({ mappedValue in (key, mappedValue) }) })
                .scan(Dictionary<Key, Result>(), accumulator: { (collectedValues: [Key: Result], newValue: (Key, Result)) in
                    var updatedCollectedValues = collectedValues
                    updatedCollectedValues[newValue.0] = newValue.1
                    return updatedCollectedValues
                })
                .startWith([:])

        return Observable.combineLatest(multicastedSelf, availableMappedValues,
                resultSelector: { (input: E, availableOutputs: [Key: Result]) -> Output in
                    let unpackedInputs = unpack(input)
                    var results = Array<Result>()
                    for unpackedInput in unpackedInputs {
                        if let result = availableOutputs[keySelector(unpackedInput)] {
                            results.append(result)
                        }
                    }
                    return repack(input, results)
                })
    }

}

func ignoreNil<A>(x: A?) -> Observable<A> {
    return x.map {
        Observable.just($0)
    } ?? Observable.empty()
}