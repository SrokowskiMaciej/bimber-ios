//
//  StringExtensions.swift
//  Bimber
//
//  Created by Maciek on 28.08.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
