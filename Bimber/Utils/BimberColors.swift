//
//  UIColors.swift
//  Bimber
//
//  Created by Maciek on 20.09.2017.
//  Copyright © 2017 bimber. All rights reserved.
//

import Foundation
import UIColor_Hex_Swift

public struct BimberColors {
    public static let colorPrimary = UIColor("#fd5c63")
    public static let colorAccent = UIColor("#fd5c63")
    public static let colorPrimaryDark = UIColor("#c42339")
    public static let colorPrimaryLight = UIColor("#ff8f91")
    public static let colorWhite = UIColor.white

    public static let colorPositiveAction = UIColor("#4CAF50")
    public static let colorNegativeAction = UIColor("#F44336")
    public static let colorNeutralAction = UIColor("#FFC107")

}

