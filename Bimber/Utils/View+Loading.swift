//
// Created by Maciej Srokowski on 5/8/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import UIKit

public extension UIView {
    func instantiateFrom(nib: UINib) -> UIView {
        return nib.instantiate(withOwner: self, options: nil)[0] as! UIView
    }

    func setupFrom(view: UIView) {
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(view)
    }

    func setupFrom(nib: UINib) {
        setupFrom(view: instantiateFrom(nib: nib))
    }
}