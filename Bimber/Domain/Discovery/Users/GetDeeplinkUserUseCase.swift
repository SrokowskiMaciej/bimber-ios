//
// Created by Maciej Srokowski on 4/18/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import RxSwift

struct GetDeeplinkUserUseCase {
    let userDeeplinkRepository: UserDeeplinkRepository
    let userProfileRepository: UserProfileRepository
    let getMatchChatIdUseCase: GetMatchStatusUseCase

    func onDeeplinkUser(currentUserId: UserId) -> Observable<DeeplinkUserResult> {
        return userDeeplinkRepository.onDeeplinkUser()
                .flatMapLatest { deeplinkUsers -> Observable<DeeplinkUserResult> in
                    switch deeplinkUsers {
                    case let users where users.isEmpty:
                        return Observable.just(DeeplinkUserResult.noDeepLinkUserResult)
                    case let users where users.first == currentUserId:
                        return Observable.just(DeeplinkUserResult.currentUserDeepLink)
                    case let users:
                        if let user = users.first {
                            return self.resolveUserProfile(currentUserId: currentUserId, deeplinkUserId: user)
                                    .startWith(DeeplinkUserResult.loadingDeepLinkUserResult)
                                    .catchError({ error in Observable.just(DeeplinkUserResult.errorDeepLinkUserResult(error: error)) })
                        } else {
                            return Observable.just(DeeplinkUserResult.noDeepLinkUserResult)
                        }
                    }

                }
    }


    private func resolveUserProfile(currentUserId: UserId, deeplinkUserId: UserId) -> Observable<DeeplinkUserResult> {
        return getMatchChatIdUseCase.onMatchStatus(currentUserId: currentUserId, userToCheckId: deeplinkUserId)
                .take(1)
                .flatMap { status -> Observable<DeeplinkUserResult> in
                    switch status {
                    case .matched(let chatId):
                        return Observable.just(DeeplinkUserResult.alreadyMatchedUserDeepLink(dialogId: chatId))
                    case .notMatched:
                        return self.userProfileRepository.onProfileData(ofUser: deeplinkUserId)
                                .take(1)
                                .map({ userData in NearbyUserData(user: userData, userLocation: nil, referenceLocation: nil) })
                                .map({ nearbyUserData in DeeplinkUserResult.successDeepLinkUserResult(deeplinkUser: nearbyUserData) })
                    }
                }
    }

    enum DeeplinkUserResult {
        case noDeepLinkUserResult
        case currentUserDeepLink
        case loadingDeepLinkUserResult
        case alreadyMatchedUserDeepLink(dialogId: String)
        case successDeepLinkUserResult(deeplinkUser: NearbyUserData)
        case errorDeepLinkUserResult(error: Error)
    }
}
