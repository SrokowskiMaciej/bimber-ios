//
// Created by Maciej Srokowski on 4/18/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import RxSwift

struct GetRecentlyRejectedUserUseCase {

    let recentlyRejectedUserRepository: RecentlyRejectedUserRepository

    func onRecentlyRejectedUser() -> Observable<RecentlyRejectedUserResult> {
        return self.recentlyRejectedUserRepository.onReversibleUser()
                .map { users -> RecentlyRejectedUserResult in
                    if let reversibleUser = users.first {
                        switch reversibleUser {
                        case .rejected(let user):
                            return RecentlyRejectedUserResult.hasRejectedUserResult(rejectedUser: user)
                        case .reverted(let user):
                            return RecentlyRejectedUserResult.hasRevertedUserResult(revertedUser: user)
                        }
                    } else {
                        return RecentlyRejectedUserResult.noRecentlyRejectedUserResult
                    }
                }

    }

    enum RecentlyRejectedUserResult {
        case noRecentlyRejectedUserResult
        case hasRejectedUserResult(rejectedUser: NearbyUserData)
        case hasRevertedUserResult(revertedUser: NearbyUserData)
    }
}
