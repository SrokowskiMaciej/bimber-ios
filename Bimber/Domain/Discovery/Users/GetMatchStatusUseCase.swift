//
// Created by Maciej Srokowski on 4/18/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import RxSwift

struct GetMatchStatusUseCase {

    let personEvaluationRepository: PersonEvaluationRepository

    func onMatchStatus(currentUserId: UserId, userToCheckId: UserId) -> Observable<MatchStatus> {
        return self.personEvaluationRepository.onEvaluatedPerson(evaluatingUserId: currentUserId, evaluatedPersonUid: userToCheckId)
                .map { (evaluation: PersonEvaluation) -> MatchStatus in
                    if case .matched = evaluation.status,
                       let chatId = evaluation.dialogId {
                        return .matched(chatId: chatId)
                    } else {
                        return .notMatched
                    }
                }
    }

    enum MatchStatus {
        case notMatched
        case matched(chatId: String)
    }
}
