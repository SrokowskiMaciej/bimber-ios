//
// Created by Maciej Srokowski on 4/18/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import RxSwift

struct GetNearbyDiscoverableUsersUseCase {

    let defaultValues: DefaultValues
    let userProfileRepository: UserProfileRepository
    let userLocationRepository: UsersLocationRepository
    let nearbyDiscoverableUsersRepository: NearbyDiscoverableUsersRepository
    let backgroundSchedulerProvider: BackgroundSchedulerProvider


    func onDiscoverableUsers(currentUserId: UserId) -> Observable<NearbyUsersResult> {
        return userLocationRepository.on(uId: currentUserId)
                .flatMapLatest { locations -> Observable<NearbyUsersResult> in
                    if let location = locations.first {
                        return self.onNearbyDiscoverableUsers(currentUserId: currentUserId, location: location)
                    } else {
                        return Observable.just(NearbyUsersResult.noLocationChosen)
                    }
                }
    }


    private func onNearbyDiscoverableUsers(currentUserId: UserId, location: UserLocation) -> Observable<NearbyUsersResult> {
        let request = NearbyDiscoverableUsersRepository.LocationRequest(uId: currentUserId,
                                                                        latitude: location.userLocationLatitude,
                                                                        longitude: location.userLocationLongitude,
                                                                        range: location.userLocationRange)
        return nearbyDiscoverableUsersRepository.onNearbyDiscoverableUsers(withLocation: request)
                .aggregateFrom(unpack: { result in
                    result.availableUsers()
                },
                               keySelector: { result in
                                   result
                               },
                               mapping: { userId in
                                   self.userDiscoverabilityData(uId: userId, referenceUserLocation: location)
                               },
                               repack: { (input: NearbyDiscoverableUsersRepository.Result, outputs: [NearbyUserData]) in
                                   switch input {
                                   case .success(_, let noMoreUsers):
                                       if noMoreUsers {
                                           return NearbyUsersResult.noMoreUsersInTheArea(remainingUsers: outputs)
                                       } else {
                                           return NearbyUsersResult.success(nearbyUsers: outputs)
                                       }
                                   case .loading:
                                       return NearbyUsersResult.loading(remainingUsers: outputs)
                                   case .error:
                                       return NearbyUsersResult.error(remainingUsers: outputs)
                                   }
                               }
                )
    }

    private func userDiscoverabilityData(uId: String, referenceUserLocation: UserLocation) -> Observable<NearbyUserData> {
        let userProfile: Observable<UserProfile> = self.userProfileRepository.onProfileData(ofUser: uId)
                .take(1)
        let existingLocation: Observable<UserLocation> = self.userLocationRepository.on(uId: uId)
                .take(1)
                .filter({ locations in !locations.isEmpty })
                .map({ locations in locations.first! })

        return Observable.zip(userProfile, existingLocation,
                              resultSelector: { (profile: UserProfile, userLocation: UserLocation) -> NearbyUserData in
                                  return NearbyUserData(user: profile, userLocation: userLocation, referenceLocation: referenceUserLocation)
                              })
                .filter({ (data: NearbyUserData) in data.user.user.displayName != DefaultValues.unknownUserName })
                .filter({ (data: NearbyUserData) in data.user.user.existing })
                .catchError({ error in Observable.empty() })
    }

    enum NearbyUsersResult {
        case noLocationChosen
        case loading(remainingUsers: [NearbyUserData])
        case success(nearbyUsers: [NearbyUserData])
        case noMoreUsersInTheArea(remainingUsers: [NearbyUserData])
        case error(remainingUsers: [NearbyUserData])
    }

}
