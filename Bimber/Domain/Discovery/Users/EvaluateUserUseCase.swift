//
// Created by Maciej Srokowski on 28/08/2018.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation
import RxSwift

struct EvaluateUserUseCase {
    let personEvaluationRepository: PersonEvaluationRepository
    let userProfileRepository: UserProfileRepository
    let nearbyDiscoverableUsersRepository: NearbyDiscoverableUsersRepository


    func likeUser(currentUserId: UserId, likedUserId: UserId) -> Single<MatchData> {
        return Single.just(.matched(userId: likedUserId))
    }

    func dislikeUser(currentUserId: UserId, dislikedUserId: UserId) -> Single<MatchData> {
        return Single.just(.notMatched(userId: dislikedUserId))
    }

    enum MatchData {
        case matched(userId: UserId)
        case notMatched(userId: UserId)
    }
}
