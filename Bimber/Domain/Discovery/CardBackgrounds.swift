//
// Created by Maciej Srokowski on 5/18/18.
// Copyright (c) 2018 bimber. All rights reserved.
//

import Foundation

enum CardBackground: UInt32 {
    case type1
    case type2
    case type3
    case type4
    case type5
    case type6

    static func randomCardBackground() -> CardBackground {
        return CardBackground(rawValue: arc4random_uniform(type6.rawValue + 1))!
    }
}

